import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://desarrollo.paneteirl.store/api/v1/apps/',
    // baseURL: 'https://produccion.paneteirl.store/api/v1/apps/',
});

export default instance;
