import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const instanceWithToken = axios.create({
  baseURL: 'https://desarrollo.paneteirl.store/api/v1/apps/',
  // baseURL: 'https://produccion.paneteirl.store/api/v1/apps/',
});

instanceWithToken.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem('token');
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instanceWithToken;