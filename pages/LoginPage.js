import React, { useState } from "react";
import { StyleSheet, ImageBackground } from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CustomButton from "../components/CustomButton";
import {
  NativeBaseProvider,
  Box,
  VStack,
  Text,
  Input,
  Icon,
  Modal,
} from "native-base";
import { Ionicons } from "@expo/vector-icons";
import BackButton from "../components/BackButton";
import Loader from "../components/Loader";
import instance from "../utils/httpService";
import CustomInput from "../components/CustomInput";

export default function LoginPage() {
  const [isLoading, setIsLoading] = useState(false);
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const login = async () => {
    setIsLoading(true);
    const notificationPush =
      (await AsyncStorage.getItem("pushTokenString")) ?? "";
    const playload = {
      user: user,
      password: password,
      role: "jalador",
      tokenPush: notificationPush,
    };
    instance
      .post("login", playload)
      .then(async (result) => {
        if (!result.data.success) {
          setErrorMessage(result.data.message);
          setErrorModalVisible(true);
          setIsLoading(false);
        } else {
          await AsyncStorage.setItem("token", result.data.data.token);
          await AsyncStorage.setItem("nombre", result.data.data.user.name);
          await AsyncStorage.setItem(
            "jaladorId",
            result.data.data.user.id.toString()
          );
          await AsyncStorage.setItem(
            "inciales",
            result.data.data.user.name.substring(0, 2).toUpperCase()
          );
          await AsyncStorage.setItem("sesion", "true");
          await AsyncStorage.setItem(
            "sucursalId",
            result.data.data.user.sucursal.id.toString()
          );
          await AsyncStorage.setItem(
            "sucursalName",
            result.data.data.user.sucursal.name
          );
          navigation.navigate("HomePage");
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.log(playload);
        console.log("Error: " + error);
      });
    setIsLoading(false);
  };

  const navigation = useNavigation();

  return (
    <NativeBaseProvider>
      {isLoading && <Loader />}
      <ImageBackground
        source={require("../assets/background2.png")}
        style={{ flex: 1 }}
      >
        <VStack
          safeArea
          flex={1}
          justifyContent="space-between"
          bg="rgba(0, 138, 190, 0.1)"
        >
          <Box flex={1}>
            <BackButton mb={20} />

            <Text style={styles.title}>Ingresar a mi Cuenta</Text>
            <Text style={styles.subTitle}>
              Bienvenido de Nuevo a PANET, para continuar ingresa con tu cuenta.
            </Text>

            <VStack
              style={{ justifyContent: "center", alignItems: "center" }}
              mt={12}
              width="100%"
              space={4}
            >
              <Box style={{ paddingVertical: 0 }}>
                <Text style={styles.labelInput}>Usuario</Text>
                <CustomInput
                  style={{ width: "100%" }}
                  value={user}
                  onChangeText={(value) => setUser(value)}
                  placeholder="Usuario"
                  icon="person"
                />
              </Box>

              <Box style={{ paddingVertical: 0 }}>
                <Text style={styles.labelInput}>Clave</Text>
                <CustomInput
                  value={password}
                  onChangeText={(value) => setPassword(value)}
                  placeholder="Clave"
                  icon="key"
                />
              </Box>
            </VStack>
          </Box>

          <Box ml={12} mr={12} style={styles.centeredBox}>
            <CustomButton onPress={login} title="Ingresar" />
          </Box>

          <Modal
            isOpen={errorModalVisible}
            onClose={() => setErrorModalVisible(false)}
          >
            <Modal.Content>
              <Modal.CloseButton />
              <Modal.Header>Error</Modal.Header>
              <Modal.Body>
                <Icon
                  as={<Ionicons name="alert-circle" />}
                  size="6xl" // Tamaño grande
                  color="red.500"
                  alignSelf="center" // Centrado horizontalmente
                  marginBottom="2" // Espacio inferior
                />
                <Text alignSelf="center">{errorMessage}</Text>
              </Modal.Body>
            </Modal.Content>
          </Modal>
        </VStack>
      </ImageBackground>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  title: {
    marginLeft: 20,
    fontSize: 20,
    marginTop: 20,
    fontWeight: "bold",
    color: "#053044",
  },
  subTitle: {
    marginLeft: 20,
    fontSize: 15,
    marginTop: 5,
    color: "#053044",
  },
  labelInput: {
    color: "#053044",
    fontWeight: "500",
    fontSize: 15,
  },
  input: {
    backgroundColor: "white",
    borderRadius: 30,
  },
});
