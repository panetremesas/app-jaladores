import { Button, Card } from "native-base";
import React from "react";
import { StyleSheet, Text } from "react-native";
import * as Clipboard from 'expo-clipboard';

export const CardAccount = ({
  titular,
  document,
  email,
  type_account,
  number,
  message,
  bank
}) => {
  const handleCopyToClipboard = async () => {
    let copiedContent = `*Titular:* ${titular}\n*Correo:* ${email}\n *Id de Cuenta:* ${number}\n *Banco:* ${bank}\n`;

    if (document) {
      copiedContent += `*Documento:* ${document}\n`;
    }

    if (type_account) {
      copiedContent += `*Tipo de Cuenta:* ${type_account}\n`;
    }

    if (message) {
      copiedContent += `*Mensaje a Colocar Obligatorio:* ${message}\n`;
    }
    await Clipboard.setStringAsync(copiedContent);
    alert("Copiado con exito");
  };

  return (
    <Card shadow={7} style={styles.card}>
      <Text style={styles.detailBoxSubTitles}>Titular</Text>
      <Text style={styles.detailBoxTitles}>{titular}</Text>
      {document && (
        <>
          <Text style={styles.detailBoxSubTitles}>Documento</Text>
          <Text style={styles.detailBoxTitles}>{document}</Text>
        </>
      )}
      <Text style={styles.detailBoxSubTitles}>Correo</Text>
      <Text style={styles.detailBoxTitles}>{email}</Text>
      {type_account && (
        <>
          <Text style={styles.detailBoxSubTitles}>Tipo de Cuenta</Text>
          <Text style={styles.detailBoxTitles}>{type_account}</Text>
        </>
      )}
      <Text style={styles.detailBoxSubTitles}>Banco</Text>
      <Text style={styles.detailBoxTitles}>{bank}</Text>

      <Text style={styles.detailBoxSubTitles}>Id de Cuenta</Text>
      <Text style={styles.detailBoxTitles}>{number}</Text>
      {message && (
        <>
          <Text style={styles.detailBoxSubTitles}>
            Mensaje a Colocar Obligatorio
          </Text>
          <Text style={styles.detailBoxTitles}>{message}</Text>
        </>
      )}

      <Button style={styles.button} onPress={handleCopyToClipboard}>
        Copiar
      </Button>
    </Card>
  );
};

const styles = StyleSheet.create({
  card: {
    width: "95%",
    borderRadius: 10,
    padding: 10,
    elevation: 5,
    backgroundColor: "white",
    marginTop: 10,
  },
  detailBoxSubTitles: {
    fontWeight: "600",
    fontSize: 16,
    fontStyle: "italic",
    marginLeft: 10,
  },
  detailBoxTitles: {
    fontSize: 14,
    marginTop: 5,
    marginLeft: 20,
  },
  button: {
    marginTop: 10,
    marginBottom: 10,
    fontWeight: "bold",
  },
});
