import { Box } from "native-base";
import React, { useEffect, useState } from "react";
import { StyleSheet, Text } from "react-native";
import CustomSelect from "../../components/CustomSelect";
import instanceWithToken from "../../utils/httpServiceWithToken";
import { CardAccount } from "./CardAccount";

export const Accounts = () => {
  const [countrie, setCountrie] = useState(null);
  const [countries, setCountries] = useState([]);
  const [accounts, setAccounts] = useState([]);
  const [banks, setBanks] = useState([]);
  const [bank, setBank] = useState(null);

  const getCountries = async () => {
    setCountries([]);
    instanceWithToken
      .get("countries?status=true")
      .then((result) => {
        const paises = [];
        result.data.data.map((countrie) => {
          paises.push({
            label: countrie.name,
            value: countrie.id,
          });
        });
        setCountries(paises);
      })
      .catch((error) => console.error(error));
  };

  const getBanks = (value) => {
    setAccounts([])
    setBank(null);
    setCountrie(value);
    setBanks([]);
    instanceWithToken.get("banks?countrie=" + value).then((result) => {
      const bancos = [];
      result.data.data.map((banco) => {
        bancos.push({
          label: banco.name,
          value: banco.id,
        });
      });
      setBanks(bancos);
    });
  };

  const getCuentas = async (value) => {
    setBank(value);
    let url = "accounts?";
    if (countrie) {
      url = url + "countrie=" + countrie;
    }

    if (bank) {
      url = url + "&bank=" + bank;
    }

    instanceWithToken
      .get(url)
      .then((result) => {
        console.log(result.data.data[0]);
        setAccounts(result.data.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  useEffect(() => {
    getCountries();
  }, []);

  return (
    <>
      <Box width="95%">
        <Text style={styles.labelInput}>Pais</Text>
        <CustomSelect
          shadow={7}
          value={countrie}
          onValueChange={(itemValue) => getBanks(itemValue)}
          placeholder="Seleccione un Pais"
          icon="arrow-down"
          items={countries}
        />
      </Box>

      <Box width="95%">
        <Text style={styles.labelInput}>Banco</Text>
        <CustomSelect
          shadow={7}
          value={bank}
          onValueChange={(itemValue) => getCuentas(itemValue)}
          placeholder="Seleccione un Banco"
          icon="arrow-down"
          items={banks}
        />
      </Box>

      {accounts.length == 0 && (
        <Box style={styles.boxEmpty}>
          <Text style={styles.textBoxEmpty}>Selecciona un pais y un banco</Text>
        </Box>
      )}

      {accounts.map((account, index) => (
        <CardAccount
          key={index}
          titular={account.titular}
          document={account.document}
          email={account.email}
          type_account={account.type_account}
          number={account.number}
          message={account.message}
          bank={account.bank.name}
        />
      ))}
    </>
  );
};

const styles = StyleSheet.create({
  labelInput: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#053044",
  },
  boxEmpty: {
    width: "95%",
    padding: 20,
    marginTop: 10,
    backgroundColor: "white",
    borderRadius: 10,
  },
  textBoxEmpty: {
    fontWeight: "bold",
    textAlign: "center",
  },
});
