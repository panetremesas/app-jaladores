import { StyleSheet, ImageBackground, ScrollView, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import BackButton from "../../components/BackButton";
import CustomButton from "../../components/CustomButton";
import instanceWithToken from "../../utils/httpServiceWithToken";
import Loader from "../../components/Loader";
import { useEffect, useState } from "react";
import CardInstrumentosDestino from "./components/CardInstrumentosDestino";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function ClientReceivePage() {
  const [isLoading, setIsLoading] = useState(false);
  const [instruments, setInstruments] = useState([]);
  const navigation = useNavigation();

  const load = async () => {
    const idClient = await AsyncStorage.getItem("trans_idClient");
    getInstruments(idClient);
  };

  const getInstruments = (idClient) => {
    setIsLoading(true);
    instanceWithToken
      .get("clients/instruments/" + idClient)
      .then((result) => {
        setInstruments(result.data.data.instruments);
        setIsLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    load();
    getInstruments();
  }, []);

  return (
    <NativeBaseProvider>
      <View style={styles.container}>
        {isLoading && <Loader />}
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          {/* <BackButton shadow={9} /> */}
          <ScrollView contentContainerStyle={styles.scrollContainer}>
            <Box width="80%" alignSelf="center" mt={5} mb={24}>
              <VStack space={4}>
                <Box width="100%">
                  <CustomButton
                    width={"95%"}
                    backgroundColor="#053044"
                    title="Agregar Instrumento Destino"
                    icon="add-circle"
                    onPress={() =>
                      navigation.navigate("AddClienInstrumentPage")
                    }
                  />
                </Box>

                <Box width="100%">
                  {instruments.map((instrument, index) => {
                    return (
                      <CardInstrumentosDestino
                        key={index}
                        tipo={instrument.type_method}
                        remitente={instrument.full_name}
                        documento={instrument.document}
                        id={instrument.id_method}
                        idRegistro={instrument.id}
                      />
                    );
                  })}
                </Box>
              </VStack>
            </Box>
          </ScrollView>
        </VStack>
        <TabButton />
      </View>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  titleStep: {
    fontSize: 20,
    fontWeight: 800,
    marginTop: 10,
    color: "#053044",
  },
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: "rgba(0, 138, 190, 0.1)",
  },
});
