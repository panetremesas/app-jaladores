import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Card, Text, Box, Image } from "native-base";
import { useNavigation } from "@react-navigation/native";
import CustomPill from "../../../components/CustomPill";
import AsyncStorage from "@react-native-async-storage/async-storage";
import instanceWithToken from "../../../utils/httpServiceWithToken";
import Cookie from 'js-cookie'
const CardInstrumentosDestino = ({
  tipo,
  remitente,
  id,
  documento,
  idRegistro,
  isRetiro = null,
}) => {
  const navigation = useNavigation();

  const handleCardPress = async (id) => {
    await AsyncStorage.setItem("trans_idInstrumento", idRegistro.toString());
    if (isRetiro) {
        const playload = {
            amount: parseFloat(await AsyncStorage.getItem('new_retiro_amount')),
            description: await AsyncStorage.getItem('new_retiro_description'),
            wallet: await AsyncStorage.getItem('new_retiro_wallet'),
            user: await AsyncStorage.getItem('new_retiro_user'),
            destination: await AsyncStorage.getItem('new_retiro_countrie'),
            instrument: idRegistro
        }
        
        console.log(playload)
        instanceWithToken.post('withdrawallet', playload).then((result) => {
          alert(result.data.message)
          navigation.navigate("WithdrawalPage")
        }).catch((e) => console.log(e))
    } else {
      navigation.navigate("PayPage", {
        parametro: idRegistro,
      });
    }
  };

  const getInstrumentos = () => {};

  return (
    <TouchableOpacity onPress={() => handleCardPress(id)} activeOpacity={0.9}>
      <Card shadow={7} style={styles.card}>
        <View style={styles.row}>
          <Box style={styles.column}>
            <Box>
              {tipo === "Transferencia" && (
                <CustomPill text={tipo} color={"yellow.400"} />
              )}
              {tipo === "PagoMovil" && (
                <CustomPill text={"Pago Movil"} color={"blue.400"} />
              )}
              {tipo === "Crypto" && (
                <CustomPill text={tipo} color={"secondary.700"} />
              )}
              <View style={styles.row}>
                <Text style={styles.boldText}>Titular:</Text>
                <Text style={styles.text}> {remitente}</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.boldText}>Documento:</Text>
                <Text style={styles.text}> {documento}</Text>
              </View>
              <View style={styles.row}>
                <Text style={styles.boldText}>Nº ó Id:</Text>
                <Text style={styles.text}> {id}</Text>
              </View>
            </Box>
          </Box>
        </View>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    padding: 5,
    elevation: 5,
    backgroundColor: "white",
    marginTop: 10,
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 5,
    paddingBottom: 5,
  },
  column: {
    flex: 1,
  },
  columntwo: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  fecha: {
    fontWeight: "normal",
    fontSize: 17,
    marginBottom: 5, // Espaciado entre los textos en la segunda columna
  },
  referencia: {
    fontWeight: "normal",
    fontSize: 17,
    marginTop: 3,
  },
  monto: {
    fontWeight: "700",
    fontSize: 20, // Reducido el tamaño del texto
  },
  boldText: {
    fontWeight: "bold",
    fontSize: 17,
  },
  text: {
    fontSize: 17,
  },
});

export default CardInstrumentosDestino;
