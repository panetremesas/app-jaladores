import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { Card, Text, Box, Image, Icon } from 'native-base';
import { useNavigation } from '@react-navigation/native';

const CardTrans = ({ referencia, fecha, status, monto, borderRadius, id, index }) => {
  const navigation = useNavigation();

  const handleCardPress = (id) => {
    navigation.navigate('DetailTransactionPage', { idTransaccion: id, create: false });
  };

  const getColor = (status) => {
    if (status == 'CREADA') return 'primary.800'
    if (status == 'VALIDACION') return 'yellow.500' 
    if (status == 'ACEPTADA') return 'green.800'
    if (status == 'RECHAZADA') return 'red.800'
    if (status == 'OBSERVADA') return 'tertiary.800'
    if (status == 'EN PROCESO') return 'warning.700'
    if (status == 'COMPLETADA') return 'teal.800'
    if (status == 'CANCELADO') return 'muted.800'
  }

  return (
    <TouchableOpacity key={index} onPress={() => handleCardPress(id)} activeOpacity={0.9}>
      <Card shadow={7} style={styles.card}>
        <View style={styles.row}>
          <Box style={styles.column}>
            <Box>
              <Text style={styles.referencia}>{referencia}</Text>
              <Box
                bg={getColor(status)}
                px={2}
                py={1}
                borderRadius={borderRadius}
                alignSelf="flex-start"
                marginRight={2}
                marginTop={2}
              >
                <Text color="white" fontWeight="bold">{status}</Text>
              </Box>
            </Box>
          </Box>
          <Box style={styles.columntwo}>
            <Box>
              <Text style={[styles.fecha, { textAlign: 'right' }]}>{fecha}</Text>
              <Text style={[styles.monto, { textAlign: 'right' }]}>{monto}</Text>
            </Box>
          </Box>
        </View>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 10,
    padding: 10,
    elevation: 5,
    backgroundColor: 'white',
    marginTop: 10
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  column: {
    flex: 1,
  },
  columntwo: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  fecha: {
    fontWeight: 'normal',
    fontSize: 17,
    marginBottom: 5 // Espaciado entre los textos en la segunda columna
  },
  referencia: {
    fontWeight: 'normal',
    fontSize: 17
  },
  monto: {
    fontWeight: '700',
    fontSize: 20 // Reducido el tamaño del texto
  }
});

export default CardTrans;
