import { StyleSheet, ImageBackground, ScrollView, Platform, Image, Pressable, View } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import BackButton from '../../components/BackButton';
import Loader from '../../components/Loader';
import { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CustomInput from "../../components/CustomInput";
import CustomButton from "../../components/CustomButton";
import * as ImagePicker from 'expo-image-picker';
import DateTimePicker from '@react-native-community/datetimepicker';
import instanceWithToken from '../../utils/httpServiceWithToken';
import CustomSelectTwoLines from "../../components/CustomSelectTwoLines";
import * as FileSystem from 'expo-file-system';

export default function PayPage() {
    const [isLoading, setIsLoading] = useState(false);
    const navigation = useNavigation();
    const [image, setImage] = useState(null);
    const [date, setDate] = useState(new Date());
    const [numero, setNumero] = useState('')
    const [showDatePicker, setShowDatePicker] = useState(false);
    const [accountss, setAccountss] = useState([])
    const [account, setAccount] = useState('')

    const pickImage = async () => {
        if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== 'granted') {
                alert('Lo siento, necesitamos permisos de la biblioteca de medios para hacer esto!');
                return;
            }
        }

        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: false,
            quality: 1,
        });

        if (!result.cancelled) {
            setImage(result.assets[0].uri);
        }
    };

    const handleDateChange = (event, selectedDate) => {
        setShowDatePicker(Platform.OS === 'ios'); // Oculta el selector en iOS después de seleccionar una fecha
        if (selectedDate) {
            setDate(selectedDate);
        }
    };

    const createTransaction = async () => {
        setIsLoading(true)
        const imageData = await FileSystem.readAsStringAsync(image, {
            encoding: FileSystem.EncodingType.Base64,
        });

        const imageFilename = 'imagen_' + Math.random().toString(36).substring(2, 15) + '.jpg';

        const imageProcesada = {
            uri: image,
            type: 'image/jpeg',
            name: imageFilename,
            data: imageData
        };

        instanceWithToken.get('transactions/validate-comprobante/' + numero + '/' + date.toISOString().split('T')[0])
            .then(async (result) => {
                if (result.data.success == false) {
                    alert("Este numero de comprobante con esta fecha ya fue enviado, ponte en contacto con administracion si consideras que es un error.")
                    setIsLoading(false)
                }
                else {
                    const idJalador = await AsyncStorage.getItem('jaladorId')
                    const idCliente = await AsyncStorage.getItem('trans_idClient')
                    const idInstrumento = await AsyncStorage.getItem('trans_idInstrumento')
                    const idRate = await AsyncStorage.getItem('trans_idRate')
                    const envio = await AsyncStorage.getItem('trans_envio')
                    const recibo = await AsyncStorage.getItem('trans_recibo')
                    const idDestino = await AsyncStorage.getItem('trans_idDestino')
                    const idOrigen = await AsyncStorage.getItem('trans_idOrigen')

                    const formData = new FormData();
                    formData.append('image', imageProcesada);
                    formData.append('creator', idJalador);
                    formData.append('client', idCliente);
                    formData.append('instrument', idInstrumento);
                    formData.append('rate', idRate);
                    formData.append('amountSend', envio);
                    formData.append('amountReceived', recibo);
                    formData.append('dateProofOfSend', date.toISOString().split('T')[0]);
                    formData.append('numberProofOfSend', numero);
                    formData.append('idDestino', parseInt(idDestino));
                    formData.append('idOrigin', parseInt(idOrigen));
                    formData.append('idJalador', parseInt(idJalador));
                    formData.append('account', account);

                    instanceWithToken.post('transactions/create', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                        .then((response) => {
                            if (response.data.success) {
                                alert("Transaccion Creada Exitosamente")
                                navigation.navigate('DetailTransactionPage', { idTransaccion: response.data.data, create: true })
                                setIsLoading(false)
                            }
                        })
                        .catch((error) => {
                            console.error('Error al crear la transacción:', error);
                            setIsLoading(false)
                        });

                }
            })


    }

    const getCuentas = async () => {
        const idOrigen = await AsyncStorage.getItem('trans_idOrigen')
        instanceWithToken.get('accounst/byCountrie/' + idOrigen).then((result) => {
            const cuentas = result.data.data.accounts
            const accounts = []

            cuentas.map((cuenta) => {
                accounts.push({
                    value: cuenta.id,
                    label: `${cuenta.titular}\n${cuenta.bank.name}\n${cuenta.number}`
                })
            })

            setAccountss(accounts)
        })
    }

    useEffect(() => {
        getCuentas();
    }, []);

    useEffect(() => {
    }, [account]);


    return (
        <NativeBaseProvider>
            <View style={styles.container}>
                {isLoading && <Loader />}
                    <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
                        {/* <BackButton shadow={9} /> */}
                        <ScrollView contentContainerStyle={styles.scrollContainer}>
                            <Box width="80%" alignSelf="center" mb={24}>

                                <Box width="100%">
                                    <Text style={styles.labelInput}>Cuenta Recepcion Destino</Text>
                                    <CustomSelectTwoLines
                                        value={account} onValueChange={(itemValue) => setAccount(itemValue)}
                                        shadow={7}
                                        placeholder="Cuenta Destino"
                                        icon="arrow-down"
                                        items={accountss}
                                    />
                                </Box>

                                <Box width="100%">
                                    <Text style={styles.labelInput}>Numero de Transaccion</Text>
                                    <CustomInput
                                        value={numero} onChangeText={(value) => setNumero(value)} keyboardType='numeric'
                                        icon="barcode-outline" width="100%" placeholder={'Numero de transaccion'} />
                                </Box>

                                <Box width="100%" mt={5}>
                                    <Text style={styles.labelInput}>Fecha de Transaccion</Text>
                                    <CustomInput
                                        icon="calendar-number-outline"
                                        width="100%"
                                        placeholder={'Fecha de Transaccion'}
                                        value={date.toLocaleDateString()} // Muestra la fecha seleccionada en el input
                                        onPressIn={() => setShowDatePicker(true)} // Muestra el selector al presionar el input
                                    />
                                    {showDatePicker && (
                                        <DateTimePicker
                                            value={date}
                                            mode="date"
                                            display="default"
                                            onChange={handleDateChange}
                                        />
                                    )}
                                </Box>

                                <Box width="100%" mt={5}>
                                    <Text style={styles.labelInput}>Comprobante</Text>
                                    {!image && <CustomButton
                                        width={'100%'}
                                        backgroundColor="#053044"
                                        title="Cargar Comprobante"
                                        icon='image'
                                        onPress={pickImage}
                                    />}
                                </Box>
                                {image && (
                                    <Pressable onPress={pickImage}>
                                        <Image
                                            source={{ uri: image }}
                                            style={{ width: '100%', height: 450, resizeMode: 'contain' }}
                                        />
                                    </Pressable>
                                )}

                                <Box width="70%" style={{ marginLeft: '15%' }}>
                                    <CustomButton
                                        onPress={createTransaction}
                                        width={'100%'}
                                        backgroundColor="#009640"
                                        title="Generar y Ver Detalle"
                                        icon='save'
                                    />
                                </Box>
                            </Box>
                        </ScrollView>
                    </VStack>
                <TabButton />
            </View>
        </NativeBaseProvider>
    );
}

const styles = StyleSheet.create({
    titleDetail: {
        fontSize: 20,
        fontWeight: 800,
        marginLeft: 20,
        marginTop: 10,
        color: '#053044',
    },
    titleStep: {
        fontSize: 20,
        fontWeight: 800,
        marginTop: 10,
        color: '#053044'
    },
    labelInput: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#053044',
    },
    scrollContainer: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        backgroundColor: "rgba(0, 138, 190, 0.1)",
      },
});