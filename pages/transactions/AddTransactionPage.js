import { StyleSheet, View, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import BackButton from "../../components/BackButton";
import CustomButton from "../../components/CustomButton";
import CustomInput from "../../components/CustomInput";
import CustomSelect from "../../components/CustomSelect";
import instanceWithToken from "../../utils/httpServiceWithToken";
import Loader from "../../components/Loader";
import { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function AddTransactionPage() {
  const [type_document, settype_document] = useState("");
  const [document, setdocument] = useState("");
  const [full_name, setfull_name] = useState("");
  const [phone, setphone] = useState("");
  const [email, setemail] = useState("");
  const [clientExist, setclientExist] = useState(false);
  const [id, setid] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();

  const load = async () => {
    const id = await AsyncStorage.getItem("jaladorId");
    setid(id);
  };

  useEffect(() => {
    load();
  }, []);

  const consultarCliente = () => {
    setIsLoading(true);
    instanceWithToken
      .get("clients/consult/" + document)
      .then(async (result) => {
        if (!result.data.success) {
          alert("Cliente no existe puedes proceder a crearlo!");
          setIsLoading(false);
        } else {
          setclientExist(true);
          setfull_name(result.data.data.full_name);
          setphone(result.data.data.phone);
          setemail(result.data.data.email);
          await AsyncStorage.setItem(
            "trans_idClient",
            result.data.data.id.toString()
          );
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
      });
  };

  const storeClient = async () => {
    setIsLoading(true);
    const playload = {
      full_name: full_name.toUpperCase(),
      document: parseInt(document),
      type_document,
      phone,
      email: email.toUpperCase(),
      jalador: parseInt(id),
    };

    instanceWithToken
      .post("clients", playload)
      .then(async (result) => {
        if (result.data.success) {
          await AsyncStorage.setItem(
            "trans_idClient",
            result.data.data.id.toString()
          );
          navigation.navigate("ClientReceivePage");
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.log(error);
        setIsLoading(false);
      });
  };

  return (
    <NativeBaseProvider>
      <View style={styles.container}>
        {isLoading && <Loader />}
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          {/* <BackButton shadow={9} /> */}
          <ScrollView contentContainerStyle={styles.scrollContainer}>
            <Box width="80%" alignSelf="center" mt={12} mb={24}>
              <VStack space={4}>
                <Box width="100%">
                  <Text style={styles.labelInput}>Tipo de Documento</Text>
                  <CustomSelect
                    shadow={7}
                    value={type_document}
                    onValueChange={(itemValue) => settype_document(itemValue)}
                    placeholder="Tipo de Documento"
                    icon="arrow-down"
                    items={[
                      { label: "Pasaporte", value: "PASAPORTE" },
                      { label: "Cedula", value: "CEDULA" },
                      {
                        label: "Carnet de Extranjeria",
                        value: "CARNET DE EXTRANJERIA",
                      },
                      {
                        label: "Carnet de Refugiado",
                        value: "CARNET DE REFUGIADO",
                      },
                      {
                        label: "Cedula de Extranjeria",
                        value: "CEDULA DE EXTRANJERIA",
                      },
                      { label: "DNI", value: "DNI" },
                      { label: "RUC", value: "RUC" },
                      { label: "PTP", value: "PTP" },
                      { label: "RIF", value: "RIF" },
                    ]}
                  />
                </Box>

                <Box width="100%">
                  <Text style={styles.labelInput}>Nº DOCUMENTO</Text>
                  <CustomInput
                    keyboardType="numeric"
                    onBlur={consultarCliente}
                    icon="id-card"
                    width="100%"
                    value={document}
                    onChangeText={(value) => setdocument(value)}
                    placeholder={"Nº DOCUMENTO"}
                  />
                </Box>
                <Box width="100%">
                  <Text style={styles.labelInput}>Nombre Completo</Text>
                  <CustomInput
                    value={full_name}
                    onChangeText={(value) => setfull_name(value)}
                    icon="person"
                    width="100%"
                    placeholder={"Nombre Completo"}
                  />
                </Box>
                <Box width="100%">
                  <Text style={styles.labelInput}>Telefono</Text>
                  <CustomInput
                    keyboardType="numeric"
                    value={phone}
                    onChangeText={(value) => setphone(value)}
                    icon="call"
                    width="100%"
                    placeholder={"Telefono"}
                  />
                </Box>
                <Box width="100%">
                  <Text style={styles.labelInput}>Correo</Text>
                  <CustomInput
                    value={email}
                    onChangeText={(value) => setemail(value)}
                    icon="mail"
                    width="100%"
                    placeholder={"Correo"}
                  />
                </Box>

                <Box width="100%">
                  <CustomButton
                    onPress={
                      clientExist
                        ? () => navigation.navigate("ClientReceivePage")
                        : storeClient
                    }
                    width={"100%"}
                    backgroundColor="#053044"
                    title={clientExist ? "Siguiente" : "Crear Cliente"}
                    icon={clientExist ? "arrow-redo" : "save"}
                  />
                </Box>
              </VStack>
            </Box>
          </ScrollView>
        </VStack>
        <TabButton />
      </View>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  titleStep: {
    fontSize: 20,
    fontWeight: 800,
    marginTop: 10,
    color: "#053044",
  },
  labelInput: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#053044",
  },
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    backgroundColor: "rgba(0, 138, 190, 0.1)",
  },
});
