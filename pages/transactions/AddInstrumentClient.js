import { StyleSheet, ImageBackground, ScrollView } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import BackButton from "../../components/BackButton";
import CustomButton from "../../components/CustomButton";
import CustomInput from "../../components/CustomInput";
import CustomSelect from "../../components/CustomSelect";
import instanceWithToken from "../../utils/httpServiceWithToken";
import Loader from "../../components/Loader";
import CustomModal from "../../components/CustomModal";
import { useState, useEffect } from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";

export default function AddClienInstrumentPage() {
  const [type_method, settype_method] = useState("");
  const [successModalVisible, setSuccessModalVisible] = useState(false);
  const [successMessage, setSuccessMessage] = useState("");
  const [type_document, settype_document] = useState("");
  const [document, setdocument] = useState("");
  const [full_name, setfull_name] = useState("");
  const [email, setEmail] = useState("");
  const [id_method, setIdMethod] = useState("");

  const handleSuccess = (message) => {
    setSuccessMessage(message);
    setSuccessModalVisible(true);
  };

  const [countries, setCountries] = useState([]);
  const [countrie, setCountrie] = useState("");
  const [banks, setBanks] = useState([]);
  const [bank, setBank] = useState("");

  const [type_method_trans, setTypeMethodTrans] = useState("");
  const type_methods_trans = [
    { label: "Corriente", value: "Corriente" },
    { label: "Ahorro", value: "Ahorro" },
  ];
  const [idJalador, setIdJalador] = useState(false);
  const [idClient, setIdClient] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();

  const load = async () => {
    instanceWithToken.get("countries?status=true").then((result) => {
      const paises = [];
      result.data.data.map((countrie) => {
        paises.push({
          label: countrie.name,
          value: countrie.id,
        });
      });
      setCountries(paises);
    });

    const id = await AsyncStorage.getItem("jaladorId");
    const idClient = await AsyncStorage.getItem("trans_idClient");
    setIdJalador(id);
    setIdClient(idClient);
  };

  const loadBanks = (itemValue) => {
    setCountrie(itemValue);
    setIsLoading(true);
    instanceWithToken.get("countries/bank/" + itemValue).then((result) => {
      const bancos = [];
      result.data.data.banks.map((bank) => {
        bancos.push({
          label: bank.name,
          value: bank.id,
        });
      });
      setBanks(bancos);
      setIsLoading(false);
    });
  };

  useEffect(() => {
    load();
  }, []);

  const storeInstrument = () => {
    setIsLoading(true);
    const playload = {
      client: parseInt(idClient),
      bank_method: bank ? bank : null,
      type_document,
      document: parseInt(document),
      full_name,
      email,
      type_method,
      id_method,
    };

    instanceWithToken
      .post("clients/instruments", playload)
      .then(async (result) => {
        handleSuccess(result.data.message);
        await AsyncStorage.setItem(
          "trans_idInstrumento",
          result.data.data.id.toString()
        );
        navigation.navigate("PayPage");
        setIsLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setIsLoading(false);
      });
  };
  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../../assets/background2.png")}
        style={{ flex: 1 }}
      >
        <CustomModal
          iconName="checkmark-done-circle"
          iconColor="#053044"
          isOpen={successModalVisible}
          onClose={() => setSuccessModalVisible(false)}
          errorMessage={successMessage}
          title="Exito"
        />
        {isLoading && <Loader />}
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <BackButton shadow={9} />
          <ScrollView contentContainerStyle={styles.scrollContainer}>
            <Text style={styles.titleDetail}>Crear Instrumento</Text>
            <Box width="80%" alignSelf="center" mt={12} mb={24}>
              <VStack space={4}>
                <Box width="100%">
                  <Text style={styles.labelInput}>Tipo De Instrumento</Text>
                  <CustomSelect
                    shadow={7}
                    value={type_method}
                    onValueChange={(itemValue) => settype_method(itemValue)}
                    placeholder="Tipo de Documento"
                    icon="arrow-down"
                    items={[
                      { label: "Transferencia", value: "Transferencia" },
                      { label: "Billetera Movil", value: "PagoMovil" },
                      { label: "Crypto", value: "Crypto" },
                    ]}
                  />
                </Box>

                {type_method != "" && (
                  <>
                    <Text style={styles.titleDetail}>Datos del Receptor</Text>
                    <Box width="100%">
                      <Text style={styles.labelInput}>Tipo de Documento</Text>
                      <CustomSelect
                        shadow={7}
                        value={type_document}
                        onValueChange={(itemValue) =>
                          settype_document(itemValue)
                        }
                        placeholder="Tipo de Documento"
                        icon="arrow-down"
                        items={[
                          { label: "Pasaporte", value: "PASAPORTE" },
                          { label: "Cedula", value: "CEDULA" },
                          {
                            label: "Carnet de Extranjeria",
                            value: "CARNET DE EXTRANJERIA",
                          },
                          {
                            label: "Carnet de Refugiado",
                            value: "CARNET DE REFUGIADO",
                          },
                          {
                            label: "Cedula de Extranjeria",
                            value: "CEDULA DE EXTRANJERIA",
                          },
                          { label: "DNI", value: "DNI" },
                          { label: "RUC", value: "RUC" },
                          { label: "PTP", value: "PTP" },
                          { label: "RIF", value: "RIF" },
                        ]}
                      />
                    </Box>

                    <Box width="100%">
                      <Text style={styles.labelInput}>Nº DOCUMENTO</Text>
                      <CustomInput
                        keyboardType="numeric"
                        icon="id-card"
                        width="100%"
                        value={document}
                        onChangeText={(value) => setdocument(value)}
                        placeholder={"Nº DOCUMENTO"}
                      />
                    </Box>

                    <Box width="100%">
                      <Text style={styles.labelInput}>Nombre Completo</Text>
                      <CustomInput
                        value={full_name}
                        onChangeText={(value) => setfull_name(value)}
                        icon="person"
                        width="100%"
                        placeholder={"Nombre Completo"}
                      />
                    </Box>

                    <Box width="100%">
                      <Text style={styles.labelInput}>Correo</Text>
                      <CustomInput
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                        icon="mail"
                        width="100%"
                        placeholder={"Correo"}
                      />
                    </Box>

                    <Text style={styles.titleDetail}>
                      Datos del Instrumento de Recepcion
                    </Text>
                  </>
                )}

                {type_method === "Transferencia" && (
                  <>
                    <Box width="100%">
                      <Text style={styles.labelInput}>Pais</Text>
                      <CustomSelect
                        shadow={7}
                        value={countrie}
                        onValueChange={(itemValue) => {
                          loadBanks(itemValue);
                        }}
                        placeholder="Seleccione un Pais"
                        icon="arrow-down"
                        items={countries}
                      />
                    </Box>

                    <Box width="100%">
                      <Text style={styles.labelInput}>Banco</Text>
                      <CustomSelect
                        shadow={7}
                        value={bank}
                        onValueChange={(itemValue) => setBank(itemValue)}
                        placeholder="Seleccione un Banco"
                        icon="arrow-down"
                        items={banks}
                      />
                    </Box>
                  </>
                )}

                {type_method === "PagoMovil" && (
                  <>
                    <>
                      <Box width="100%">
                        <Text style={styles.labelInput}>Pais</Text>
                        <CustomSelect
                          shadow={7}
                          value={countrie}
                          onValueChange={(itemValue) => {
                            loadBanks(itemValue);
                          }}
                          placeholder="Seleccione un Pais"
                          icon="arrow-down"
                          items={countries}
                        />
                      </Box>

                      <Box width="100%">
                        <Text style={styles.labelInput}>Banco</Text>
                        <CustomSelect
                          shadow={7}
                          value={bank}
                          onValueChange={(itemValue) => setBank(itemValue)}
                          placeholder="Seleccione un Banco"
                          icon="arrow-down"
                          items={banks}
                        />
                      </Box>
                    </>
                  </>
                )}

                {type_method === "Transferencia" && (
                  <>
                    <Box width="100%">
                      <Text style={styles.labelInput}>Tipo Cuenta</Text>
                      <CustomSelect
                        shadow={7}
                        value={type_method_trans}
                        onValueChange={(itemValue) =>
                          setTypeMethodTrans(itemValue)
                        }
                        placeholder="Tipo de Cuenta"
                        icon="arrow-down"
                        items={type_methods_trans}
                      />
                    </Box>
                  </>
                )}

                {type_method != "" && (
                  <>
                    <Box width="100%">
                      <Text style={styles.labelInput}>
                        {type_method === "Transferencia" && "Numero de Cuenta"}
                        {type_method === "PagoMovil" && "Numero de Telefono"}
                        {type_method === "Crypto" && "Id del Wallet"}
                      </Text>
                      <CustomInput
                        keyboardType="numeric"
                        icon="id-card"
                        width="100%"
                        value={id_method}
                        onChangeText={(value) => setIdMethod(value)}
                      />
                    </Box>

                    <CustomButton
                      onPress={storeInstrument}
                      width={"100%"}
                      backgroundColor="#053044"
                      title={"Crear Instrumento"}
                      icon={"save"}
                    />
                  </>
                )}
              </VStack>
            </Box>
          </ScrollView>
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
  },
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  titleStep: {
    fontSize: 20,
    fontWeight: 800,
    marginTop: 10,
    color: "#053044",
  },
  labelInput: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#053044",
  },
});
