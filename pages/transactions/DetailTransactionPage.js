import React, { useEffect, useState } from "react";
import { StyleSheet, Dimensions } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeBaseProvider, VStack, Box, Button } from "native-base";
import { Text } from "@rneui/themed";
import TabButton from "../../components/TabButton";
import BackButton from "../../components/BackButton";
import instanceWithToken from "../../utils/httpServiceWithToken";
import Loader from "../../components/Loader";
import CustomPill from "../../components/CustomPill";
import * as Clipboard from "expo-clipboard";

export default function DetailTransactionPage({ route }) {
  const navigation = useNavigation();
  const { idTransaccion, create } = route.params;
  const [trackins, setTrackings] = useState([]);
  const [transaccion, setTransaccion] = useState({
    reference: "",
    amountReceived: "",
    currencyReceived: "",
    instrument: {
      id_method: "",
      full_name: "",
    },
    createdAt: "",
    status: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const getTransaction = () => {
    setIsLoading(true);
    instanceWithToken.get("transactions/" + idTransaccion).then((result) => {
      setTransaccion(result.data.data);
      setIsLoading(false);
    });
  };

  const handleCardPress = () => {
    navigation.navigate("TrackingTransactionPage", {
      idTransaccion: idTransaccion,
    });
  };

  useEffect(() => {
    getTransaction();
  }, []);

  const copy = async () => {
    await Clipboard.setStringAsync('https://expedient.paneteirl.com/'+transaccion.expedient.id);
    alert("Expediente Copiado con exito");
  };
  return (
    <NativeBaseProvider>
      <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
        {create == false && <BackButton shadow={9} />}
        {isLoading && <Loader />}
        <Text style={styles.titleDetail}>Detalle de Transaccion</Text>
        <Box style={styles.container}>
          <Text style={styles.referencia}>{transaccion.reference}</Text>
          <Text h2 style={styles.monto}>
            {transaccion.amountReceived} {transaccion.currencyReceived}
          </Text>

          <Box
            shadow={9}
            style={[
              styles.detailBox,
              { width: Dimensions.get("window").width * 0.8 },
            ]}
          >
            <Text style={styles.detailBoxTitles}>Monto Enviado</Text>
            <Text style={styles.detailBoxSubTitles}>
              {transaccion.amountSend} {transaccion.currencySend}
            </Text>
            <Text style={styles.detailBoxTitles}>Nº Cuenta Receptor</Text>
            <Text style={styles.detailBoxSubTitles}>
              {transaccion.instrument.id_method}
            </Text>
            <Text style={styles.detailBoxTitles}>Titular</Text>
            <Text style={styles.detailBoxSubTitles}>
              {transaccion.instrument.full_name}
            </Text>
            <Text style={styles.detailBoxTitles}>Fecha</Text>
            <Text style={styles.detailBoxSubTitles}>
              {transaccion.createdAt.split("T")[0]}
            </Text>
            <Text style={styles.detailBoxTitles}>Ganancia</Text>
            <Text style={styles.detailBoxSubTitles}>
              {transaccion.puller_profit} {transaccion.currencySend}
            </Text>
            <Text style={styles.detailBoxTitles}>Estado</Text>
            <Text style={styles.detailBoxSubTitles}>
              {transaccion.status == "CREADA" && (
                <CustomPill color="primary.800" text={transaccion.status} />
              )}
              {transaccion.status == "VALIDACION" && (
                <CustomPill color="yellow.500" text={transaccion.status} />
              )}
              {transaccion.status == "ACEPTADA" && (
                <CustomPill color="green.800" text={transaccion.status} />
              )}
              {transaccion.status == "RECHAZADA" && (
                <CustomPill color="red.800" text={transaccion.status} />
              )}
              {transaccion.status == "OBSERVADA" && (
                <CustomPill color="tertiary.800" text={transaccion.status} />
              )}
              {transaccion.status == "EN PROCESO" && (
                <CustomPill color="warning.700" text={transaccion.status} />
              )}
            </Text>
            {transaccion.status == "OBSERVADA" && (
              <>
                <Text style={styles.detailBoxTitles}>Mensaje de Observacion</Text>
                <Text style={styles.detailBoxSubTitles}>
                  {transaccion.observation}
                </Text>
              </>
            )}
          </Box>

          {transaccion.status == "OBSERVADA" && (
            <Button
              onPress={copy}
              fontWeight="bold"
              width={80}
              mt="2"
              mb="10%"
              borderRadius={30}
              backgroundColor="#053044"
              _text={{ fontWeight: "bold" }}
            >
              Copiar Link Para Expediente
            </Button>
          )}
        </Box>

        <Box ml={12} mr={12} style={styles.centeredBox}>
          <Button
            onPress={() => {
              handleCardPress();
            }}
            fontWeight="bold"
            width={80}
            mt="2"
            mb="10%"
            borderRadius={30}
            backgroundColor="#053044"
            _text={{ fontWeight: "bold" }}
          >
            Tracking
          </Button>
        </Box>
      </VStack>
      <TabButton />
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  referencia: {
    fontSize: 22,
    fontStyle: "italic",
  },
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: 15,
  },
  monto: {
    fontWeight: 800,
    fontSize: 24,
  },
  detailBox: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    marginTop: 20,
  },
  detailBoxTitles: {
    fontWeight: "700",
    fontSize: 18,
    fontStyle: "italic",
    marginTop: 5,
  },
  detailBoxSubTitles: {
    fontWeight: "600",
    fontSize: 16,
    fontStyle: "italic",
    marginLeft: 10,
  },
  white: {
    color: "white",
  },
});
