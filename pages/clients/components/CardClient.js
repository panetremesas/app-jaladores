import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { Card, Text, Box, Image } from 'native-base';
import { useNavigation } from '@react-navigation/native';

const CardClient = ({ title, number, document, id, typeDocument, index }) => {
  const navigation = useNavigation();

  const handleCardPress = (id) => {
    navigation.navigate('DetailClientPagePage', { idClient: id});
  };

  return (
    <TouchableOpacity key={index} onPress={() => handleCardPress(id)} activeOpacity={1}>
      <Card style={styles.card}>
        <View style={styles.row}>
          {/* Lado izquierdo */}
          <View style={[styles.column, { flex: 0.9 }]}>
            <View>
              <Text style={styles.textDocument}>{typeDocument} {document}</Text>
              <Text style={styles.text}>{title}</Text>
            </View>
          </View>
          {/* Lado derecho */}
          <View style={[styles.column, { flex: 0.2, alignItems: 'flex-end' }]}>
            <Box
              bg={'green.800'}
              px={2}
              py={1}
              borderRadius={30}
              alignSelf="flex-end"
            >
              <Text color="white" fontWeight="bold">{number}</Text>
            </Box>
          </View>
        </View>
      </Card>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textDocument: {
    fontWeight: 600
  },
  card: {
    borderRadius: 10,
    padding: 10,
    elevation: 5,
    backgroundColor: 'white',
    marginTop: 10
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  column: {
    flex: 1,
  },
  columntwo: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  fecha: {
    fontWeight: 'normal',
    fontSize: 17,
    marginBottom: 5 // Espaciado entre los textos en la segunda columna
  },
  referencia: {
    fontWeight: 'normal',
    fontSize: 17
  },
  monto: {
    fontWeight: '700',
    fontSize: 20 // Reducido el tamaño del texto
  }
});

export default CardClient;
