import React, { useState } from 'react';
import { StyleSheet, View, ScrollView, ImageBackground } from "react-native";
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useEffect } from 'react';
import Loader from '../../components/Loader';
import {
    Avatar,
    NativeBaseProvider,
    VStack,
    Text,
} from "native-base";
import TabButton from "../../components/TabButton";
import ProfileButton from "../../components/ProfileButton";
import instanceWithToken from '../../utils/httpServiceWithToken';
import CardClient from './components/CardClient';

export default function ClientsPage() {
    const [isLoading, setIsLoading] = useState(false)
    const [userName, setUserName] = useState('')
    const [iniciales, setIniciales] = useState('')
    const [clients, setClients] = useState([])

    // validar sesion
    const validarSesion = async () => {
        const sesion = await AsyncStorage.getItem('sesion');
        if (sesion !== "true") {
            navigation.navigate('WelcomePage')
        }
        else {
            setUserName(await AsyncStorage.getItem('nombre'))
            setIniciales(await AsyncStorage.getItem('iniciales'))
        }
    }

    const getClients = () => {
        setIsLoading(true)
        instanceWithToken.get('getClients/puller').then((result) => {
            setClients(result.data.data)
            console.log(result.data.data)
            setIsLoading(false)
        })

    }

    useEffect(() => {
        validarSesion();
        getClients()
    }, []);
    // validar sesion
    const navigation = useNavigation();

    return (
        <NativeBaseProvider>
            <ImageBackground
                source={require("../../assets/background2.png")}
                style={{ flex: 1 }}
            >
                <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
                    <ProfileButton shadow={9} />
                    <Text style={styles.titleDetail}>Tus Clientes</Text>
                    {isLoading && <Loader />}
                    <ScrollView contentContainerStyle={styles.scrollContainer}>
                        <View style={styles.container} >
                            {clients.map((client, index) => {
                                return (
                                    <View style={styles.containerTrans} key={index}>
                                        <CardClient
                                            index={index}
                                            typeDocument={client.type_document}
                                            title={client.full_name}
                                            number={client.transactionsCount}
                                            document={client.document}
                                            id={client.id}
                                        />
                                    </View>
                                )
                            })
                            }

                        </View>
                    </ScrollView>
                </VStack>
                <TabButton />
            </ImageBackground>
        </NativeBaseProvider>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },
    userName: {
        fontWeight: 'bold',
        fontSize: 22,
        marginTop: 10,
        marginBottom: 10,
        color: '#053044'
    },
    containerTrans: {
        width: '90%', // Ancho del contenedor al 90% del total
        paddingBottom: 5
    },
    titleDetail: {
        fontSize: 20,
        fontWeight: 800,
        marginLeft: 20,
        marginTop: 10,
        color: '#053044',
    },
});
