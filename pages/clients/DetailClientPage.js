import React, { useState, useEffect } from 'react';
import { NativeBaseProvider, VStack, Box, Text } from "native-base";
import { StyleSheet, Dimensions, View, ScrollView } from "react-native";
import Loader from '../../components/Loader';
import BackButton from '../../components/BackButton';
import instanceWithToken from '../../utils/httpServiceWithToken';
import CardTrans from '../transactions/components/CardTrans';
import TabButton from "../../components/TabButton";

export const DetailClientPagePage = ({ route }) => {

    const [isLoading, setIsLoading] = useState(false)
    const [client, setClient] = useState({
        full_name: '',
        document: '',
        type_document: '',
        transactions: []
    })
    const { idClient } = route.params;
    const getData = () => {
        setIsLoading(true)
        instanceWithToken.get('client/detail/' + idClient).then((result) => {
            setClient(result.data.data)
            console.info(result.data.data)
            setIsLoading(false)
        }).catch((e) => {
            console.error(e)
            setIsLoading(false)
        })
    }

    useEffect(() => {
        getData()
    }, [])
    return (
        <NativeBaseProvider>
            <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
                {isLoading && <Loader />}
                <BackButton shadow={9} />
                <Text style={styles.titleDetail}>Detalle del Cliente</Text>

                <Box shadow={9} ml={'10%'} mt={5} style={[styles.detailBox, { width: Dimensions.get('window').width * 0.8 }]}>
                    <Text style={styles.detailBoxTitles}>Documento</Text>
                    <Text style={styles.detailBoxSubTitles}> {client.type_document} {client.document}</Text>
                    <Text style={styles.detailBoxTitles}>Cliente</Text>
                    <Text style={styles.detailBoxSubTitles}>{client.full_name}</Text>
                    <Text style={styles.detailBoxTitles}>Ctda. Transacciones</Text>
                    <Text style={styles.detailBoxSubTitles}>{client.transactions.length}</Text>
                </Box>

                <Text style={styles.trans}>Sus Transacciones</Text>
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    {client.transactions.map((transaction, index) => {
                        return (
                            <View key={index} style={styles.containerTrans}>
                                <CardTrans
                                    index={index}
                                    referencia={transaction.reference}
                                    fecha={transaction.dateProofOfSend}
                                    monto={`${transaction.amountReceived} ${transaction.currencyReceived}`}
                                    id={transaction.id}
                                    status={transaction.status}
                                    borderRadius={'full'}
                                />
                            </View>)
                    })}
                </ScrollView>
                <TabButton />
            </VStack>
        </NativeBaseProvider>
    )
}



const styles = StyleSheet.create({
    titleDetail: {
        fontSize: 20,
        fontWeight: 800,
        marginLeft: 20,
        marginTop: 10,
        color: '#053044',
    },
    referencia: {
        fontSize: 22,
        fontStyle: 'italic',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: -38
    },
    monto: {
        fontWeight: 800,
        fontSize: 24,
    },
    detailBox: {
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 10,
    },
    detailBoxTitles: {
        fontWeight: '700',
        fontSize: 18,
        fontStyle: 'italic',
    },
    detailBoxSubTitles: {
        fontSize: 15,
        marginLeft: 10
    },
    white: {
        color: 'white'
    },
    containerTrans: {
        width: '80%',
        marginLeft: '10%'
    },
    trans: {
        marginLeft: '15%',
        marginTop: 20,
        fontSize: 18,
        fontWeight: 600
    },
    scrollContainer: {
        flexGrow: 1,
    },
});
