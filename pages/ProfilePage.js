import React, { useState, useEffect } from 'react';
import { StyleSheet, View, ScrollView, ImageBackground } from "react-native";
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
    Avatar,
    NativeBaseProvider,
    VStack,
    Text, Box, Center
} from "native-base";
import TabButton from "../components/TabButton";
import BackButton from '../components/BackButton';
import CustomButton from '../components/CustomButton';
import CustomInput from '../components/CustomInput';
import instanceWithToken from '../utils/httpServiceWithToken';
import Loader from '../components/Loader';
import CustomModal from '../components/CustomModal';

export default function ProfilePage() {

    const [isLoading, setIsLoading] = useState(false);
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const [iniciales, setIniciales] = useState('')
    const [errorModalVisible, setErrorModalVisible] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const [successModalVisible, setSuccessModalVisible] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');

    const handleError = (message) => {
        setErrorMessage(message);
        setErrorModalVisible(true);
    };

    const handleSuccess = (message) => {
        setSuccessMessage(message)
        setSuccessModalVisible(true)
    }

    // validar sesion
    const validarSesion = async () => {
        const sesion = await AsyncStorage.getItem('sesion');
        setIniciales(await AsyncStorage.getItem('iniciales'))
        if (sesion !== "true") {
            navigation.navigate('WelcomePage')
        }
    }

    const getProfile = () => {
        setIsLoading(true)
        instanceWithToken.get('profile').then((result) => {
            setName(result.data.data.name)
        }).catch((error) => {
            handleError(error.message)
        })
        setIsLoading(false)
    }

    useEffect(() => {
        getProfile()
        validarSesion();
    }, []);
    
    const navigation = useNavigation();

    const logout = async () => {
        await AsyncStorage.clear()
        navigation.navigate('WelcomePage')
    }

    const updateProfile = () => {
        setIsLoading(true)
        const playload = {
            name: name,
            password: password
        }


        instanceWithToken.post('updateProfile', playload)
            .then((result) => {
                if (result.data.success) {
                    handleSuccess(result.data.message)
                    setName(result.data.data.name)
                    logout()
                }
            })
            .catch((error) => {
                handleError(error.message)
            })
        setIsLoading(false)
    }

    return (
        <NativeBaseProvider>
            <ImageBackground
                source={require("../assets/background2.png")}
                style={{ flex: 1 }}
            >
                <CustomModal
                    isOpen={errorModalVisible}
                    onClose={() => setErrorModalVisible(false)}
                    errorMessage={errorMessage}
                />

                <CustomModal
                    iconName="checkmark-done-circle"
                    iconColor='#053044'
                    isOpen={successModalVisible}
                    onClose={() => setSuccessModalVisible(false)}
                    errorMessage={successMessage}
                    title='Exito'
                />


                <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
                    {isLoading && <Loader />}
                    <BackButton shadow={9} />

                    <ScrollView contentContainerStyle={styles.scrollContainer}>
                        <View style={styles.container}>
                            <Avatar bg="rgba(3, 75, 113, 0.7)" alignSelf="center" size="2xl">
                                {iniciales}
                            </Avatar>
                            <Text style={styles.userName}>Editar Perfil</Text>

                            <VStack alignContent={'center'} justifyContent={'center'} mt={12} mb={24} width="85%" marginLeft={5} space={4}>

                                <Box>
                                    <Text style={styles.labelInput}>Nombre</Text>
                                    <CustomInput icon="person" onChangeText={(value) => setName(value)} value={name} placeholder={'Nombre'} />
                                </Box>

                                <Box>
                                    <Text style={styles.labelInput}>Clave</Text>
                                    <CustomInput icon="key" onChangeText={(value) => setPassword(value)} placeholder={'Clave'} />
                                </Box>

                                <CustomButton
                                    width={'95%'}
                                    onPress={updateProfile}
                                    backgroundColor="#053044"
                                    title="Guardar"
                                    icon="save"
                                />
                            </VStack>

                            <CustomButton
                                width='80%'
                                onPress={logout}
                                backgroundColor="#BE1823"
                                title="Cerrar Sesion"
                                icon="exit"
                            />
                        </View>
                    </ScrollView>
                </VStack>
                <TabButton />
            </ImageBackground>
        </NativeBaseProvider>
    );
}

const styles = StyleSheet.create({
    scrollContainer: {
        flexGrow: 1,
    },
    container: {
        flex: 1,
        alignItems: 'center',
    },
    userName: {
        fontWeight: 'bold',
        fontSize: 22,
        marginTop: 10,
        marginBottom: 10,
        color: '#053044'
    },
    containerTrans: {
        width: '90%',
        paddingBottom: 30
    },
    labelInput: {
        color: '#053044',
        fontWeight: '500',
        fontSize: 15
    },
});
