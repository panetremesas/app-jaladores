import React, { useEffect, useState } from "react";
import { StyleSheet, Dimensions } from "react-native";
import { NativeBaseProvider, VStack, Box, Button } from "native-base";
import { Text } from "@rneui/themed";
import BackButton from "../../components/BackButton";
import instanceWithToken from "../../utils/httpServiceWithToken";
import Loader from "../../components/Loader";
import CustomPill from "../../components/CustomPill";
import * as Clipboard from 'expo-clipboard';

export const DetalleRefillPage = ({ route }) => {
  const [isLoading, setIsLoading] = useState(false);
  const { idTransaccion, create } = route.params;

  const [refill, setRefill] = useState({
    id: "",
    amount: "",
    countrie: {
      currency: "",
    },
  });

  const getRefill = () => {
    console.log("iniciando")
    instanceWithToken.get("wallets-refills/"+idTransaccion).then((result) => {
        setRefill(result.data.data)
        console.log(result.data.data)
    }).catch((e) => {
        console.log(e)
    })
  };

  const copiarExpediente = async () => {
    await Clipboard.setStringAsync('https://expedient.paneteirl.com/'+refill.expedient.id);
    alert("Expediente Copiado con exito");
  }

  useEffect(() => {
    getRefill();
  }, []);
  return (
    <NativeBaseProvider>
      <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
        {create == false && <BackButton shadow={9} />}
        {isLoading && <Loader />}
        <Text style={styles.titleDetail}>Detalle de Recarga</Text>
        <Box style={styles.container}>
          <Text style={styles.referencia}>RFP-{refill.id}</Text>
          <Text h2 style={styles.monto}>
            {refill.amount} {refill.countrie.currency}
          </Text>

          <Box
            shadow={9}
            style={[
              styles.detailBox,
              { width: Dimensions.get("window").width * 0.8 },
            ]}
          >
            <Text style={styles.detailBoxTitles}>Pais Wallet</Text>
            <Text style={styles.detailBoxSubTitles}>
              {refill.countrie.name}
            </Text>
            {/* <Text style={styles.detailBoxTitles}>Titular</Text>
            <Text style={styles.detailBoxSubTitles}>
              {refill.instrument.full_name}
            </Text> */}
            <Text style={styles.detailBoxTitles}>Fecha de Recarga</Text>
            <Text style={styles.detailBoxSubTitles}>
              {refill.dateProof}
            </Text>
            <Text style={styles.detailBoxTitles}>Numero de Comprobante</Text>
            <Text style={styles.detailBoxSubTitles}>
                {refill.numberProof}
            </Text>
            <Text style={styles.detailBoxTitles}>Estado</Text>
            <Text style={styles.detailBoxSubTitles}>
              {refill.status == "CREADA" && (
                <CustomPill color="primary.800" text={refill.status} />
              )}
              {refill.status == "VALIDACION" && (
                <CustomPill color="yellow.500" text={refill.status} />
              )}
              {refill.status == "ACEPTADA" && (
                <CustomPill color="green.800" text={refill.status} />
              )}
              {refill.status == "RECHAZADA" && (
                <CustomPill color="red.800" text={refill.status} />
              )}
              {refill.status == "OBSERVADA" && (
                <CustomPill color="tertiary.800" text={refill.status} />
              )}
              {refill.status == "EN PROCESO" && (
                <CustomPill color="warning.700" text={refill.status} />
              )}
            </Text>


            <Button onPress={copiarExpediente} style={styles.button}>Copiar Expediente</Button>
          </Box>
        </Box>
      </VStack>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  referencia: {
    fontSize: 22,
    fontStyle: "italic",
  },
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: 15,
  },
  monto: {
    fontWeight: 800,
    fontSize: 24,
  },
  detailBox: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 10,
    marginTop: 20,
  },
  detailBoxTitles: {
    fontWeight: "700",
    fontSize: 18,
    fontStyle: "italic",
    marginTop: 5,
  },
  detailBoxSubTitles: {
    fontWeight: "600",
    fontSize: 16,
    fontStyle: "italic",
    marginLeft: 10,
  },
  white: {
    color: "white",
  },
  button: {
    marginTop: 10,
    marginBottom: 10
  }
});
