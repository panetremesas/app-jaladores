import {
  StyleSheet,
  ImageBackground,
  ScrollView,
  Platform,
  Image,
  Pressable,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import BackButton from "../../components/BackButton";
import Loader from "../../components/Loader";
import { useState, useEffect } from "react";
import CustomInput from "../../components/CustomInput";
import CustomButton from "../../components/CustomButton";
import * as ImagePicker from "expo-image-picker";
import DateTimePicker from "@react-native-community/datetimepicker";
import instanceWithToken from "../../utils/httpServiceWithToken";
import CustomSelectTwoLines from "../../components/CustomSelectTwoLines";
import CustomSelect from "../../components/CustomSelect";
import * as FileSystem from "expo-file-system";

export default function WalletFillPage() {
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const [image, setImage] = useState(null);
  const [date, setDate] = useState(new Date());
  const [numero, setNumero] = useState("");
  const [monto, setMonto] = useState("");
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [accountss, setAccountss] = useState([]);
  const [account, setAccount] = useState("");
  const [countries, setCountries] = useState([]);
  const [countrie, setCountrie] = useState([]);
  const [dataBase, setDataBase] = useState([]);

  const pickImage = async () => {
    if (Platform.OS !== "web") {
      const {
        status,
      } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== "granted") {
        alert(
          "Lo siento, necesitamos permisos de la biblioteca de medios para hacer esto!"
        );
        return;
      }
    }

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: false,
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.assets[0].uri);
    }
  };

  const handleDateChange = (event, selectedDate) => {
    setShowDatePicker(Platform.OS === "ios"); // Oculta el selector en iOS después de seleccionar una fecha
    if (selectedDate) {
      setDate(selectedDate);
    }
  };

  const getCuentas = async (idOrigen) => {
    setCountrie(idOrigen);
    instanceWithToken.get("accounst/byCountrie/" + idOrigen).then((result) => {
      const cuentas = result.data.data.accounts;
      const accounts = [];

      cuentas.map((cuenta) => {
        accounts.push({
          value: cuenta.id,
          label: `${cuenta.titular}\n${cuenta.bank.name}\n${cuenta.number}`,
        });
      });

      setAccountss(accounts);
    });
  };

  const getCountries = async () => {
    setIsLoading(true);
    instanceWithToken
      .get("countries?status=true")
      .then((result) => {
        const paises = [];
        result.data.data.map((countrie) => {
          paises.push({
            label: countrie.name,
            value: countrie.id,
          });
        });
        setDataBase(result.data.data);
        setCountries(paises);
        setIsLoading(false);
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    getCountries();
  }, []);

  const walletRefill = async () => {
    setIsLoading(true);
    const imageData = await FileSystem.readAsStringAsync(image, {
      encoding: FileSystem.EncodingType.Base64,
    });

    const imageFilename =
      "imagen_" + Math.random().toString(36).substring(2, 15) + ".jpg";

    const imageProcesada = {
      uri: image,
      type: "image/jpeg",
      name: imageFilename,
      data: imageData,
    };

    const formData = new FormData();
    formData.append("image", imageProcesada);
    formData.append("countrie", countrie);
    formData.append("amount", monto);
    formData.append("account", account);
    formData.append("numberProof", numero);
    formData.append("dateProof", date.toISOString().split("T")[0]);

    instanceWithToken
      .post("wallets-refills", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((response) => {
        console.log("Realizado");
        alert("Recarga Creada Exitosamente");
        navigation.navigate("HistoryRefill");
        setIsLoading(false);
      })
      .catch((error) => {
        console.error("Error al crear la transacción:", error);
        setIsLoading(false);
      });
  };

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../../assets/background2.png")}
        style={{ flex: 1 }}
      >
        {isLoading && <Loader />}
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <BackButton shadow={9} />
          <ScrollView contentContainerStyle={styles.scrollContainer}>
            <Text style={styles.titleDetail}>Realizar Recarga</Text>

            <Box width="80%" alignSelf="center" mb={24}>
              <Box width="100%">
                <Text style={styles.labelInput}>Pais Wallet</Text>
                <CustomSelect
                  shadow={7}
                  value={countrie}
                  onValueChange={(itemValue) => getCuentas(itemValue)}
                  placeholder="Seleccione un Pais"
                  icon="arrow-down"
                  items={countries}
                />
              </Box>

              <Box width="100%">
                <Text style={styles.labelInput}>Monto de Recarga</Text>
                <CustomInput
                  value={monto}
                  onChangeText={(value) => setMonto(value)}
                  keyboardType="numeric"
                  icon="cash-outline"
                  width="100%"
                  placeholder={"Monto de la Recarga"}
                />
              </Box>

              <Box width="100%">
                <Text style={styles.labelInput}>Cuenta Recepcion Destino</Text>
                <CustomSelectTwoLines
                  value={account}
                  onValueChange={(itemValue) => setAccount(itemValue)}
                  shadow={7}
                  placeholder="Cuenta Destino"
                  icon="arrow-down"
                  items={accountss}
                />
              </Box>

              <Box width="100%">
                <Text style={styles.labelInput}>Numero de Transaccion</Text>
                <CustomInput
                  value={numero}
                  onChangeText={(value) => setNumero(value)}
                  keyboardType="numeric"
                  icon="barcode-outline"
                  width="100%"
                  placeholder={"Numero de transaccion"}
                />
              </Box>

              <Box width="100%" mt={5}>
                <Text style={styles.labelInput}>Fecha de Transaccion</Text>
                <CustomInput
                  icon="calendar-number-outline"
                  width="100%"
                  placeholder={"Fecha de Transaccion"}
                  value={date.toLocaleDateString()} // Muestra la fecha seleccionada en el input
                  onPressIn={() => setShowDatePicker(true)} // Muestra el selector al presionar el input
                />
                {showDatePicker && (
                  <DateTimePicker
                    value={date}
                    mode="date"
                    display="default"
                    onChange={handleDateChange}
                  />
                )}
              </Box>

              <Box width="100%" mt={5}>
                <Text style={styles.labelInput}>Comprobante</Text>
                {!image && (
                  <CustomButton
                    width={"100%"}
                    backgroundColor="#053044"
                    title="Cargar Comprobante"
                    icon="image"
                    onPress={pickImage}
                  />
                )}
              </Box>
              {image && (
                <Pressable onPress={pickImage}>
                  <Image
                    source={{ uri: image }}
                    style={{
                      width: "100%",
                      height: 450,
                      resizeMode: "contain",
                    }}
                  />
                </Pressable>
              )}

              <Box width="70%" style={{ marginLeft: "15%" }}>
                <CustomButton
                  onPress={walletRefill}
                  width={"100%"}
                  backgroundColor="#009640"
                  title="Generar y Ver Detalle"
                  icon="save"
                />
              </Box>
            </Box>
          </ScrollView>
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  titleStep: {
    fontSize: 20,
    fontWeight: 800,
    marginTop: 10,
    color: "#053044",
  },
  labelInput: {
    fontSize: 16,
    fontWeight: "bold",
    color: "#053044",
  },
  scrollContainer: {
    flexGrow: 1,
  },
});
