import {
  StyleSheet,
  ImageBackground,
  ScrollView,
  View,
  RefreshControl,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import BackButton from "../../components/BackButton";
import CustomButton from "../../components/CustomButton";
import instanceWithToken from "../../utils/httpServiceWithToken";
import Loader from "../../components/Loader";
import { useCallback, useEffect, useState } from "react";
import CardRefillsHistory from "./components/CardRefillsHistory";

export const HistoryRefill = () => {
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const [refills, setRefills] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const getRefills = async () => {
    setIsLoading(true);
    await instanceWithToken
      .get("wallets-refills")
      .then(async (result) => {
        await setRefills(result.data.data);
        await console.log(refills);
        await setIsLoading(false);
      })
      .catch((e) => {
        navigation.navigate("HomePage");
        setIsLoading(false);
      });
  };

  useEffect(() => {
    getRefills();
  }, []);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    getRefills()
      .then(() => setRefreshing(false))
      .catch(() => setRefreshing(false));
  }, []);

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../../assets/background2.png")}
        style={{ flex: 1 }}
      >
        {isLoading && <Loader />}
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <BackButton shadow={9} />
          <Text style={styles.titleDetail}>Historial de Recargas</Text>
          <ScrollView
            contentContainerStyle={styles.scrollContainer}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
            <Box width="80%" alignSelf="center" mt={5} mb={24}>
              <VStack space={4}>
                <Box width="100%">
                  <CustomButton
                    width={"95%"}
                    backgroundColor="#053044"
                    title="Nueva Recarga"
                    icon="add-circle"
                    onPress={() => navigation.navigate("WalletFillPage")}
                  />
                </Box>
                {refills.map((refill, index) => (
                  <View style={styles.containerTrans} key={index}>
                    <CardRefillsHistory
                      status={refill.status}
                      borderRadius={"full"}
                      referencia={`RFP-${refill.id}`}
                      fecha={refill.dateProof}
                      monto={`${refill.amount} ${refill.countrie.currency}`}
                      id={refill.id}
                    />
                  </View>
                ))}
              </VStack>
            </Box>
          </ScrollView>
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  titleStep: {
    fontSize: 20,
    fontWeight: 800,
    marginTop: 10,
    color: "#053044",
  },
  scrollContainer: {
    flexGrow: 1,
  },
  containerTrans: {
    width: "95%", // Ancho del contenedor al 90% del total
    paddingBottom: 5,
  },
});
