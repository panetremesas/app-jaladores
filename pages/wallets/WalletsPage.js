import React, { useState } from "react";
import { StyleSheet, View, ScrollView, ImageBackground } from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect } from "react";
import Loader from "../../components/Loader";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import ProfileButton from "../../components/ProfileButton";
import instanceWithToken from "../../utils/httpServiceWithToken";
import CardWallet from "./components/CardWallet";
import CustomSelect from "../../components/CustomSelect";
import CustomButton from "../../components/CustomButton";

export default function WalletsPage() {
  const [isLoading, setIsLoading] = useState(false);
  const [type_wallet, setTypeWallet] = useState("PROFITS");
  const [clients, setClients] = useState([]);
  const [userId, setUserId] = useState(0);

  // validar sesion
  const validarSesion = async () => {
    const sesion = await AsyncStorage.getItem("sesion");
    if (sesion !== "true") {
      navigation.navigate("WelcomePage");
    }
  };

  const getWallets = async () => {
    setIsLoading(true);
    const userId = await AsyncStorage.getItem("jaladorId");
    instanceWithToken
      .get("wallet-users?type=" + type_wallet + "&userId=" + userId)
      .then((result) => {
        setClients(result.data.data);
        console.log(result.data.data);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    validarSesion();
    getWallets();
  }, []);

  const loadWallets = async (item) => {
    setTypeWallet(item);
    const userId = await AsyncStorage.getItem("jaladorId");
    console.log(userId);
    instanceWithToken
      .get("wallet-users?type=" + item + "&userId=" + userId)
      .then((result) => {
        setClients(result.data.data);
        setIsLoading(false);
      });
  };

  const navigation = useNavigation();

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../../assets/background2.png")}
        style={{ flex: 1 }}
      >
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <ProfileButton shadow={9} />
          <Text style={styles.titleDetail}>Tus Wallets</Text>
          {isLoading && <Loader />}

          <Box width="90%" style={{ marginLeft: "5%" }}>
            <Text style={styles.labelInput}>Tipo de wallet</Text>
            <CustomSelect
              shadow={7}
              value={type_wallet}
              onValueChange={(itemValue) => loadWallets(itemValue)}
              placeholder="Tipo de Documento"
              icon="arrow-down"
              items={[
                { label: "GANANCIAS", value: "PROFITS" },
                { label: "RECARGAS", value: "USER" },
              ]}
            />
          </Box>

          {type_wallet == 'PROFITS' &&
          <Box width="95%" style={{ marginLeft: "5%" }}>
            <CustomButton
              width={"95%"}
              backgroundColor="#053044"
              title="Retiros"
              icon="list-circle"
              onPress={() => {navigation.navigate('WithdrawalPage')}}
            />
          </Box>}

          <ScrollView contentContainerStyle={styles.scrollContainer}>
            <View style={styles.container}>
              {clients.map((client, index) => {
                return (
                  <View key={index} style={styles.containerTrans}>
                    <CardWallet
                      pais={client.countrie.name}
                      number={client.available}
                      currency={client.countrie.currency}
                    />
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  userName: {
    fontWeight: "bold",
    fontSize: 22,
    marginTop: 10,
    marginBottom: 10,
    color: "#053044",
  },
  containerTrans: {
    width: "90%", // Ancho del contenedor al 90% del total
    paddingBottom: 5,
  },
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
});
