import React, { useState, useEffect, useCallback } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  ImageBackground,
  RefreshControl,
} from "react-native";
import { useNavigation, useFocusEffect } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from "../components/Loader";
import { Avatar, NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../components/TabButton";
import ProfileButton from "../components/ProfileButton";
import instanceWithToken from "../utils/httpServiceWithToken";
import { Accounts } from "./components/Accounts";

const HomePage = () => {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  const [userName, setUserName] = useState("");
  const [iniciales, setIniciales] = useState("");
  const [isInitialized, setIsInitialized] = useState(false);
  const [refreshing, setRefreshing] = useState(false);

  const [countries, setCountries] = useState([]);

  const validarSesion = useCallback(async () => {
    const sesion = await AsyncStorage.getItem("sesion");
    if (sesion !== "true") {
      navigation.navigate("WelcomePage");
    } else {
      const nombre = await AsyncStorage.getItem("nombre");
      setUserName(nombre);

      if (nombre) {
        const initials = nombre
          .split(" ")
          .map((word) => word.charAt(0).toUpperCase())
          .join("");
        setIniciales(initials);
        console.log(iniciales);
      }
    }
  }, [navigation]);

  useEffect(() => {
    validarSesion();
  }, []);

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../assets/background2.png")}
        style={{ flex: 1 }}
      >
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <ProfileButton shadow={9} />
          {isLoading && <Loader />}
          <ScrollView contentContainerStyle={styles.scrollContainer}>
            <View style={styles.container}>
              <Avatar bg="rgba(3, 75, 113, 0.7)" alignSelf="center" size="2xl">
                {iniciales}
              </Avatar>
              <Text style={styles.userName}>{userName}</Text>

              <Box width={"90%"} style={{ marginLeft: "5%" }}>
                <Accounts />
              </Box>
            </View>
          </ScrollView>
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  userName: {
    fontWeight: "bold",
    fontSize: 22,
    marginTop: 10,
    marginBottom: 10,
    color: "#053044",
  },
  containerTrans: {
    width: "90%", // Ancho del contenedor al 90% del total
    paddingBottom: 5,
  },
});

export default HomePage;
