import { StyleSheet, ImageBackground } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { useEffect } from 'react'; 
import AsyncStorage from '@react-native-async-storage/async-storage';
import CustomButton from '../components/CustomButton';
import {
  NativeBaseProvider,
  Box,
  Button,
  VStack,
  Text,
} from "native-base";

export default function WelcomePage() {

  const validarSesion = async () => {
    const sesion = await AsyncStorage.getItem('sesion');
    if(sesion == "true")
    {
      navigation.navigate('HomePage')
    }
  }

  useEffect(() => {
    validarSesion();
  }, []); 

  const navigation = useNavigation();

  const handleNavigation = () => {
    navigation.navigate('LoginPage');
  };

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../assets/background.png")}
        style={{ flex: 1 }}
      >
        <VStack safeArea flex={1} justifyContent="space-between">
          <Box flex={1} ml={12} mr={12}>

            <VStack space={3} mt="24">
              {/* Aquí van otros elementos de tu interfaz */}
            </VStack>
          </Box>
          <Box ml={12} mr={12} style={styles.centeredBox}>
            <Text style={styles.titulo} mb={24}>Intermediarios</Text>
            <CustomButton onPress={handleNavigation} title="Ingresar" backgroundColor="#053044" />

          </Box>
        </VStack>
      </ImageBackground>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  titulo: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  centeredBox: {
    alignItems: 'center',
    justifyContent: 'center'
  }
});
