import React, { useState, useCallback } from "react";
import { StyleSheet, View, ScrollView, ImageBackground } from "react-native";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useEffect } from "react";
import Loader from "../../components/Loader";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import TabButton from "../../components/TabButton";
import ProfileButton from "../../components/ProfileButton";
import instanceWithToken from "../../utils/httpServiceWithToken";
import CustomButton from "../../components/CustomButton";
import { CardWithdrall } from "./components/CardWithdrall";

export const WithdrawalPage = () => {
  const navigation = useNavigation();

  const [isLoading, setIsLoading] = useState(false);
  const [retiros, setRetiros] = useState([]);

  const getRetiros = async () => {
    setIsLoading(true);
    const userId = await AsyncStorage.getItem("jaladorId");
    instanceWithToken
      .get("withdrawallet?userId=" + userId)
      .then((result) => {
        setRetiros(result.data.data);
        setIsLoading(false);
      })
      .catch((e) => setIsLoading(true));
  };

  useFocusEffect(
    useCallback(() => {
      getRetiros();
      return () => {
        // aqui puedo poner algo al salir de la vista
      };
    }, [])
  );

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../../assets/background2.png")}
        style={{ flex: 1 }}
      >
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <ProfileButton shadow={9} />
          <Text style={styles.titleDetail}>Listado de Retiros</Text>
          {isLoading && <Loader />}

          <Box width="95%" style={{ marginLeft: "5%" }}>
            <CustomButton
              width={"95%"}
              backgroundColor="#053044"
              title="Solicitar Retiro"
              icon="add-circle"
              onPress={() => {
                navigation.navigate("NewWithdrawalPage");
              }}
            />
          </Box>

          <Box width="100%">
            {retiros.map((retiro, index) => (
              <CardWithdrall
                monto={retiro.amount}
                moneda={retiro.wallet.countrie.currency}
                status={retiro.status}
                key={index}
              />
            ))}
          </Box>
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  userName: {
    fontWeight: "bold",
    fontSize: 22,
    marginTop: 10,
    marginBottom: 10,
    color: "#053044",
  },
  containerTrans: {
    width: "90%", // Ancho del contenedor al 90% del total
    paddingBottom: 5,
  },
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
});
