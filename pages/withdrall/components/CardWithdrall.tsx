import { Card, Text } from "@rneui/base";
import React from "react";
import { StyleSheet, View } from "react-native";
import CustomPill from "../../../components/CustomPill";

export const CardWithdrall = ({ monto, moneda, status }) => {
  return (
    <Card containerStyle={styles.cardContainer}>
      <View style={styles.container}>
        <Text style={styles.label}>Monto</Text>
        <Text style={styles.amount}>
          {monto} {moneda}
        </Text>
      </View>
      <View style={styles.pillContainer}>
        {status === "CREADA" && (
          <CustomPill color="primary.800" text={status} />
        )}
        {status === "ACEPTADA" && (
          <CustomPill color="green.800" text={status} />
        )}
        {status === "RECHAZADA" && <CustomPill color="red.800" text={status} />}
        {status === "OBSERVADA" && (
          <CustomPill color="tertiary.800" text={status} />
        )}
      </View>
    </Card>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  label: {
    fontWeight: "bold",
  },
  amount: {
    fontWeight: "bold",
  },
  pillContainer: {
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  cardContainer: {
    borderRadius: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
