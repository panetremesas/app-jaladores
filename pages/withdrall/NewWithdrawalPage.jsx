import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  ImageBackground,
  TextInput,
  Platform,
} from "react-native";
import { useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from "../../components/Loader";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import CustomSelectTwoLines from "../../components/CustomSelectTwoLines";
import CustomButton from "../../components/CustomButton";
import instanceWithToken from "../../utils/httpServiceWithToken";
import TabButton from "../../components/TabButton";
import CustomSelect from "../../components/CustomSelect";
import Cookies from 'js-cookie'

export const NewWithdrawalPage = () => {
  const navigation = useNavigation();

  const [isLoading, setIsLoading] = useState(false);
  const [walletsBase, setWalletsBase] = useState([]);
  const [wallets, setWallets] = useState([]);
  const [countries, setCountries] = useState([]);
  const [countrie, setCountrie] = useState([]);
  const [wallet, setWallet] = useState(null);
  const [amount, setAmount] = useState("0"); 
  const [walletBase, setWalletBase] = useState({
    countrie: {
      currency: "",
    },
  });
  const [description, setDescription] = useState("");

  const getCountries = async () => {
    setCountries([]);
    instanceWithToken
      .get("countries?status=true")
      .then((result) => {
        const paises = [];
        result.data.data.map((countrie) => {
          paises.push({
            label: countrie.name,
            value: countrie.id,
          });
        });
        setCountries(paises);
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    const getWallets = async () => {
      setIsLoading(true);
      const userId = await AsyncStorage.getItem("jaladorId");
      instanceWithToken
        .get(`wallet-users?type=PROFITS&userId=${userId}`)
        .then((result) => {
          const walletss = result.data.data.map((wallet) => ({
            value: wallet.id,
            label: `${wallet.countrie.name}\n${wallet.available} ${wallet.countrie.currency}`,
          }));
          setWalletsBase(result.data.data);
          setWallets(walletss);
          setIsLoading(false);
        })
        .catch((error) => {
          console.error("Error fetching wallets:", error);
          setIsLoading(false);
        });
    };
    getCountries();
    getWallets();
  }, []);

  const loadBase = (value) => {
    setWallet(value);
    const foundWallet = walletsBase.find((w) => w.id === value);
    setWalletBase(foundWallet || { countrie: { currency: "" } });
  };

  const handleAmountChange = (value) => {
    const enteredAmount = parseFloat(value);
    if (
      !isNaN(enteredAmount) &&
      enteredAmount <= parseFloat(walletBase.available)
    ) {
      setAmount(value);
    } else {
      setAmount(walletBase.available.toString()); // Ensure amount is a string for TextInput
    }
  };

  const storeWithdrawal = async () => {
    const userId = await AsyncStorage.getItem("jaladorId");
    await AsyncStorage.setItem('new_retiro_wallet', walletBase.id.toString())
    await AsyncStorage.setItem('new_retiro_countrie', countrie.toString())
    await AsyncStorage.setItem('new_retiro_amount', parseFloat(amount).toString())
    await AsyncStorage.setItem('new_retiro_description', description)
    await AsyncStorage.setItem('new_retiro_user', userId.toString())

    navigation.navigate('SelectInstrumentPage')
  };
  

  return (
    <NativeBaseProvider>
      <View
        style={{ flex: 1 }}
      >
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          {/* <BackButton shadow={9} /> */}

          {isLoading && <Loader />}

          <Box width="90%" style={{ marginLeft: "5%" }}>
            <Text style={styles.labelInput}>Pais Destino</Text>
            <CustomSelect
              shadow={7}
              value={countrie}
              onValueChange={(itemValue) => setCountrie(itemValue)}
              placeholder="Seleccione un Pais Dest¡no"
              icon="arrow-down"
              items={countries}
            />
          </Box>

          <Box width="90%" style={{ marginLeft: "5%" }}>
            <Text style={styles.labelInput}>Seleccione Wallet de Retiro</Text>
            <CustomSelectTwoLines
              value={wallet}
              onValueChange={loadBase}
              shadow={7}
              placeholder="Cuenta Destino"
              icon="arrow-down"
              items={wallets}
            />
          </Box>

          <View
            style={{
              height: 80,
              width: "90%",
              marginLeft: "5%",
              flexDirection: "row",
              alignItems: "center",
              marginTop: 24,
            }}
          >
            <Box
              bg="white"
              p={3}
              flex={0.75}
              borderTopLeftRadius={10}
              borderBottomLeftRadius={10}
              height={20}
            >
              <VStack>
                <Text style={{ color: "#053044" }}>Monto</Text>
                <TextInput
                  onChangeText={handleAmountChange}
                  value={amount}
                  style={{
                    height: 40,
                    borderColor: "gray",
                    borderWidth: 0,
                    fontWeight: "bold",
                    fontSize: 20,
                  }}
                  keyboardType="numeric"
                />
              </VStack>
            </Box>
            <Box
              bg="#053044"
              justifyContent="center"
              alignItems="center"
              flex={0.25}
              borderTopRightRadius={10}
              borderBottomRightRadius={10}
              p={3}
              height={20}
            >
              <Text style={styles.moneda}>{walletBase.countrie.currency}</Text>
            </Box>
          </View>

          <Box width={"90%"} style={{ marginLeft: "5%" }}>
            <TextInput
              multiline
              numberOfLines={10}
              placeholder="Introduce tus datos de retiro"
              textAlignVertical="top"
              value={description}
              onChangeText={setDescription}
              style={[
                styles.textInput,
                Platform.OS === "ios" ? styles.shadowIOS : styles.shadowAndroid,
              ]}
            />
          </Box>

          <Box width="95%" style={{ marginLeft: "5%" }}>
            <CustomButton
              width={"95%"}
              backgroundColor="#053044"
              title="Solicitar Retiro"
              icon="add-circle"
              onPress={storeWithdrawal}
            />
          </Box>
        </VStack>
        <TabButton />
      </View>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({
  titleDetail: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
  labelInput: {
    fontSize: 16,
    marginBottom: 10,
    marginTop: 20,
    color: "#053044",
  },
  moneda: {
    color: "white",
    fontWeight: "bold",
    fontSize: 20,
  },
  textInput: {
    backgroundColor: "white",
    marginTop: 15,
    borderRadius: 20,
    padding: 15,
    ...Platform.select({
      ios: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.3,
        shadowRadius: 4,
      },
      android: {
        elevation: 4,
      },
    }),
  },
});

export default NewWithdrawalPage;
