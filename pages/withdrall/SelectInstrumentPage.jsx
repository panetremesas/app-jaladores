import { Button, Input, NativeBaseProvider } from "native-base";
import React, { useState } from "react";
import { Text, View, StyleSheet } from "react-native";
import Loader from "../../components/Loader";
import instanceWithToken from "../../utils/httpServiceWithToken";
import CardInstrumentosDestino from "../transactions/components/CardInstrumentosDestino";

const SelectInstrumentPage = () => {
  const [instruments, setInstruments] = useState([]);
  const [document, setDocument] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  const getInstruments = (idClient) => {
    setIsLoading(true);
    instanceWithToken
      .get("clients/instruments/" + document + "?document=true")
      .then((result) => {
        setInstruments(result.data.data.instruments);
        setIsLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setIsLoading(false);
      });
  };

  return (
    <NativeBaseProvider>
      {isLoading && <Loader />}
      <View style={styles.container}>
        <View style={styles.box}>
          <Text>Documento del Cliente</Text>
          <Input
            value={document}
            onChangeText={setDocument}
            placeholder="Documento"
            style={styles.TextInput}
          />
          <Button style={styles.Button} onPress={getInstruments}>
            Consultar Instrumentos
          </Button>

          <View width="100%">
            {instruments.map((instrument, index) => {
              return (
                <CardInstrumentosDestino
                  key={index}
                  tipo={instrument.type_method}
                  remitente={instrument.full_name}
                  documento={instrument.document}
                  id={instrument.id_method}
                  idRegistro={instrument.id}
                  isRetiro={true}
                />
              );
            })}
          </View>
        </View>
      </View>
    </NativeBaseProvider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgba(0, 138, 190, 0.1)",
  },
  box: {
    width: "90%",
    marginLeft: "5%",
  },
  TextInput: {
    backgroundColor: "white",
    padding: 5,
    paddingLeft: 10,
  },
  Button: {
    marginTop: 10,
    backgroundColor: "#053044",
  },
});

export default SelectInstrumentPage;
