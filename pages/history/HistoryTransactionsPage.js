import React, { useState, useCallback } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  ImageBackground,
  RefreshControl,
} from "react-native";
import { useFocusEffect, useNavigation } from "@react-navigation/native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Loader from "../../components/Loader";
import { NativeBaseProvider, VStack, Text } from "native-base";
import TabButton from "../../components/TabButton";
import ProfileButton from "../../components/ProfileButton";
import CardTrans from "../transactions/components/CardTrans";
import instanceWithToken from "../../utils/httpServiceWithToken";

export default function HistoryTransactionsPage() {
  const [isLoading, setIsLoading] = useState(false);
  const [userName, setUserName] = useState("");
  const [iniciales, setIniciales] = useState("");
  const [transactions, setTransactions] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  // validar sesion
  const validarSesion = async () => {
    const sesion = await AsyncStorage.getItem("sesion");
    if (sesion !== "true") {
      navigation.navigate("WelcomePage");
    } else {
      setUserName(await AsyncStorage.getItem("nombre"));
      setIniciales(await AsyncStorage.getItem("iniciales"));
    }
  };

  const getTransactionsInit = useCallback(async () => {
    setIsLoading(true);
    instanceWithToken.get("getTransactions/puller").then((result) => {
      setTransactions(result.data.data.slice(0, 30));
      setIsLoading(false);
    });
  }, []);

  const onRefresh = useCallback(() => {
    setRefreshing(true);
    getTransactionsInit()
      .then(() => setRefreshing(false))
      .catch(() => setRefreshing(false));
  }, []);

  useFocusEffect(
    useCallback(() => {
      console.log("ejecutando")
      validarSesion();
      getTransactionsInit();
      return () => {
        // aqui puedo poner algo al salir de la vista
      };
    }, [])
  );

  // validar sesion
  const navigation = useNavigation();

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../../assets/background2.png")}
        style={{ flex: 1 }}
      >
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <ProfileButton shadow={9} />
          <Text style={styles.titleDetail}>Historial de Transacciones</Text>
          {isLoading && <Loader />}
          <ScrollView
            contentContainerStyle={styles.scrollContainer}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          >
            <View style={styles.container}>
              {transactions.map((transaction, index) => {
                return (
                  <View key={index} style={styles.containerTrans}>
                    <CardTrans
                      status={transaction.status}
                      key={index}
                      referencia={transaction.reference}
                      fecha={transaction.dateProofOfSend}
                      monto={`${transaction.amountReceived} ${transaction.currencyReceived}`}
                      id={transaction.id}
                      borderRadius={30}
                    />
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    alignItems: "center",
  },
  userName: {
    fontWeight: "bold",
    fontSize: 22,
    marginTop: 10,
    marginBottom: 10,
    color: "#053044",
  },
  containerTrans: {
    width: "90%", // Ancho del contenedor al 90% del total
    paddingBottom: 5,
  },
  titleDetail: {
    fontSize: 20,
    fontWeight: 800,
    marginLeft: 20,
    marginTop: 10,
    color: "#053044",
  },
});
