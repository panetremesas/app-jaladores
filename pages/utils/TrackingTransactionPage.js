import React, { useEffect, useState } from 'react';
import { StyleSheet, Dimensions, RefreshControl } from "react-native";
import { useNavigation } from '@react-navigation/native';
import {
    NativeBaseProvider,
    VStack, Box, Button,
} from "native-base";
import { Text } from '@rneui/themed';
import TabButton from "../../components/TabButton";
import BackButton from '../../components/BackButton';
import instanceWithToken from '../../utils/httpServiceWithToken';
import Loader from '../../components/Loader';
import Timeline from 'react-native-timeline-flatlist'

export default function TrackingTransactionPage({ route }) {
    const navigation = useNavigation();
    const { idTransaccion } = route.params;
    const [trackins, setTracking] = useState([])
    const [transaccion, setTransaccion] = useState({
        reference: '',
        amountReceived: '',
        currencyReceived: '',
        instrument: {
            id_method: '',
            full_name: ''
        },
        createdAt: '',
        status: ''
    })
    const [isLoading, setIsLoading] = useState(false);

    const getTransaction = () => {
        setIsLoading(true)
        instanceWithToken.get('transactions/' + idTransaccion).then((result) => {
            setTransaccion(result.data.data)
            const tracks = []

            result.data.data.trackings.map((track) => {
                tracks.push({
                    time: track.fecha,
                    title: track.status
                })
            })
            setTracking(tracks)
            setIsLoading(false)
        })
    }

    const renderFooter = () => {
        if (this.state.waiting) {
            return <ActivityIndicator />;
        } else {
            return <Text>~</Text>;
        }
    }

    useEffect(() => {
        getTransaction()
    }, [])

    return (
        <NativeBaseProvider>
            <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
                <BackButton shadow={9} />
                {isLoading && <Loader />}
                <Text style={styles.titleDetail}>Tracking de Transaccion</Text>
                <Box style={styles.container}>
                    <Text style={styles.referencia}>{transaccion.reference}</Text>
                    <Text h2 style={styles.monto}>{transaccion.amountReceived} {transaccion.currencyReceived}</Text>

                    <Timeline
                        style={{ marginTop: 10, width: '80%', backgroundColor: 'white', padding: 10, borderRadius: 30, height: 'auto' }}
                        timeContainerStyle={{ minWidth: 52 }}
                        circleStyle={{ marginTop: 10 }}
                        timeStyle={{ textAlign: 'center', backgroundColor: '#ff9797', color: 'white', padding: 5, borderRadius: 13 }}
                        data={trackins}
                        titleStyle={{ color: 'black' }}
                        innerCircle={'dot'}
                    />
                </Box>

            </VStack>
            <TabButton />
        </NativeBaseProvider>
    );
}

const styles = StyleSheet.create({
    titleDetail: {
        fontSize: 20,
        fontWeight: 800,
        marginLeft: 20,
        marginTop: 10,
        color: '#053044',
    },
    referencia: {
        fontSize: 22,
        fontStyle: 'italic',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 15,
    },
    monto: {
        fontWeight: 800,
        fontSize: 24,
    },
    detailBox: {
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 10,
        marginTop: 20
    },
    detailBoxTitles: {
        fontWeight: '700',
        fontSize: 18,
        fontStyle: 'italic',
        marginTop: 5
    },
    detailBoxSubTitles: {
        fontWeight: '600',
        fontSize: 16,
        fontStyle: 'italic',
        marginLeft: 10
    },
    white: {
        color: 'white'
    }
});
