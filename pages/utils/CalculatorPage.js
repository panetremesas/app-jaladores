import { StyleSheet, ImageBackground, View, TextInput } from "react-native";
import { NativeBaseProvider, VStack, Text, Box } from "native-base";
import { useNavigation } from "@react-navigation/native";
import TabButton from "../../components/TabButton";
import BackButton from "../../components/BackButton";
import Loader from "../../components/Loader";
import { useEffect, useState } from "react";
import instanceWithToken from "../../utils/httpServiceWithToken";
import CustomSelect from "../../components/CustomSelect";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CustomButton from "../../components/CustomButton";

export default function CalculatorPage() {
  const [isLoading, setIsLoading] = useState(false);
  const [countries, setCountries] = useState([]);
  const [destinos, setDestinos] = useState([]);
  const [destino, setDestino] = useState([]);
  const [countrie, setCountrie] = useState([]);
  const [dataBase, setDataBase] = useState([]);
  const [ratesOriginales, setRatesOriginales] = useState([]);
  const [rateSeleccionada, setRateSeleccionada] = useState(false);
  const [rateMonto, setRateMonto] = useState(false);
  const [dataTase, setDataTase] = useState("");
  const [showCalculo, setShowCalculo] = useState(false);
  const navigation = useNavigation();
  const [envias, setEnvias] = useState("");
  const [recibes, setRecibes] = useState("");

  const getCountries = async () => {
    setIsLoading(true);
    instanceWithToken
      .get("countries?status=true")
      .then((result) => {
        const paises = [];
        result.data.data.map((countrie) => {
          paises.push({
            label: countrie.name,
            value: countrie.id,
          });
        });
        setDataBase(result.data.data);
        setCountries(paises);
        setIsLoading(false);
      })
      .catch((error) => console.error(error));
  };

  const loadDestinos = (countrie) => {
    setIsLoading(true);
    setCountrie(countrie);
    instanceWithToken
      .get("rates/origen/actives/" + countrie)
      .then((result) => {
        const paises = [];
        setRatesOriginales(result.data.data);
        result.data.data.map((countried) => {
          if (countrie !== countried.destination.id) {
            paises.push({
              label: countried.destination.name,
              value: countried.destination.id,
            });
          }
        });
        setDestinos(paises);
        setDestino("");
        setShowCalculo(false);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const locateRate = (origen, destino) => {
    const foundRate = ratesOriginales.find(
      (rate) => rate.origin.id === origen && rate.destination.id === destino
    );

    if (foundRate) {
      return foundRate;
    } else {
      return null;
    }
  };

  const loadRate = (countried) => {
    setDestino(countried);
    const resultado = locateRate(countrie, countried);
    setRateMonto(resultado.rate);
    setRateSeleccionada(resultado.id);
    setDataTase(resultado);
    setEnvias(null);
    setRecibes(null);
    setShowCalculo(true);
  };

  const asignarEnvio = (value) => {
    let result = null;
    setEnvias(value);
    const monto = parseFloat(value) || 0;
    if (countrie == 1 && destino == 2) {
      result = monto * rateMonto;
    }
    if (countrie === 1 && destino !== 2) {
      result = monto / rateMonto;
    }

    if (countrie == 2 && destino == 1) {
      result = monto / rateMonto;
    }
    if (countrie == 2 && destino == 1) {
      result = monto / rateMonto;
    }

    if (countrie !== 1 && countrie !== 2) {
      result = monto * rateMonto;
    }

    result = result.toFixed(2);
    setRecibes(result.toString());
  };

  const asignarRecibes = (value) => {
    let result = null;
    setRecibes(value);
    const monto = parseFloat(value) || 0;

    if (destino === 1 && countrie === 2) {
      result = monto * rateMonto;
    }
    if (destino === 1 && countrie !== 2) {
      result = monto / rateMonto;
    }

    if (destino == 2 && countrie == 1) {
      result = monto / rateMonto;
    }
    if (destino == 2 && countrie == 1) {
      result = monto / rateMonto;
    }

    if (destino !== 1 && destino !== 2) {
      result = monto / rateMonto;
    }

    result = result.toFixed(2);
    setEnvias(result.toString());
  };

  const setDatos = async () => {
    await AsyncStorage.setItem("trans_idOrigen", dataTase.origin.id.toString());
    await AsyncStorage.setItem(
      "trans_idDestino",
      dataTase.destination.id.toString()
    );
    await AsyncStorage.setItem("trans_idRate", rateSeleccionada.toString());
    await AsyncStorage.setItem("trans_envio", envias.toString());
    await AsyncStorage.setItem("trans_recibo", recibes.toString());
    navigation.navigate("PayPage");
  };
  
  useEffect(() => {
    getCountries();
  }, []);

  return (
    <NativeBaseProvider>
      <ImageBackground
        source={require("../../assets/background2.png")}
        style={{ flex: 1 }}
      >
        {isLoading && <Loader />}
        <VStack safeArea flex={1} bg="rgba(0, 138, 190, 0.1)">
          <BackButton shadow={9} />
          <Box width="80%" alignSelf="center" mt={5}>
            <VStack space={4}>
              <Box width="100%">
                <Text style={styles.title}>Calcula tu Transaccion</Text>
                <Text style={styles.titleStep}>Origen</Text>
                <CustomSelect
                  shadow={7}
                  value={countrie}
                  onValueChange={(itemValue) => loadDestinos(itemValue)}
                  placeholder="Seleccione un Pais"
                  icon="arrow-down"
                  items={countries}
                />
              </Box>

              {destinos.length > 0 && (
                <Box width="100%">
                  <Text style={styles.titleStep}>Destino</Text>
                  <CustomSelect
                    shadow={7}
                    value={destino}
                    onValueChange={(itemValue) => loadRate(itemValue)}
                    placeholder="Seleccione un Pais"
                    icon="arrow-down"
                    items={destinos}
                  />
                </Box>
              )}
            </VStack>
          </Box>

          {showCalculo && (
            <>
              <View
                shadow={9}
                style={{
                  height: 80,
                  width: "80%",
                  marginLeft: "10%",
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 24,
                }}
              >
                <Box
                  shadow={9}
                  bg="white"
                  p={3}
                  flex={0.75}
                  borderTopLeftRadius={10}
                  borderBottomLeftRadius={10}
                  height={20}
                >
                  <VStack>
                    <Text style={{ color: "#053044" }}>Tu Envias</Text>
                    <TextInput
                      onChangeText={(value) => asignarEnvio(value)}
                      value={envias}
                      style={{
                        height: 40,
                        borderColor: "gray",
                        borderWidth: 0,
                        fontWeight: "bold",
                        fontSize: 20,
                      }}
                      keyboardType="numeric"
                    />
                  </VStack>
                </Box>
                <Box
                  shadow={9}
                  bg="#053044"
                  justifyContent="center"
                  alignItems="center"
                  flex={0.25}
                  borderTopRightRadius={10}
                  borderBottomRightRadius={10}
                  p={3}
                  height={20}
                >
                  <Text style={styles.moneda}>{dataTase.origin.currency}</Text>
                </Box>
              </View>

              <View
                style={{
                  height: 80,
                  width: "80%",
                  marginLeft: "10%",
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 10,
                }}
              >
                <Box
                  shadow={9}
                  bg="white"
                  p={3}
                  flex={0.75}
                  borderTopLeftRadius={10}
                  borderBottomLeftRadius={10}
                  height={20}
                >
                  <VStack>
                    <Text style={{ color: "#053044" }}>Tu Recibes</Text>
                    <TextInput
                      onChangeText={(value) => asignarRecibes(value)}
                      value={recibes}
                      style={{
                        height: 40,
                        borderColor: "gray",
                        borderWidth: 0,
                        fontWeight: "bold",
                        fontSize: 20,
                      }}
                      keyboardType="numeric"
                    />
                  </VStack>
                </Box>
                <Box
                  shadow={9}
                  bg="#053044"
                  justifyContent="center"
                  alignItems="center"
                  flex={0.25}
                  borderTopRightRadius={10}
                  borderBottomRightRadius={10}
                  p={3}
                  height={20}
                >
                  <Text style={styles.moneda}>
                    {dataTase.destination.currency}
                  </Text>
                </Box>
              </View>
              {/* aqui quiero un texto a la derecha */}
              <Text
                style={{
                  textAlign: "right",
                  marginRight: "13%",
                  marginTop: 10,
                  color: "#053044",
                  fontWeight: 600,
                }}
              >
                Tipo de Cambio Usada: {dataTase.rate}
              </Text>

              <Box width="70%" style={{ marginLeft: "15%", marginTop: 40 }}>
                <CustomButton
                  width={"100%"}
                  backgroundColor="#053044"
                  title="Siguiente"
                  icon="arrow-redo"
                  onPress={setDatos}
                />
              </Box>
            </>
          )}
        </VStack>
        <TabButton />
      </ImageBackground>
    </NativeBaseProvider>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 22,
    fontWeight: 500,
    marginTop: 10,
    marginLeft: 15,
    color: "#053044",
  },
  titleStep: {
    fontSize: 18,
    fontWeight: 500,
    marginTop: 10,
    marginLeft: 15,
    color: "#053044",
  },
  input: {
    height: 40,
    borderWidth: 0,
    borderColor: "white",
  },
  moneda: {
    color: "white",
    fontWeight: 700,
    fontSize: 20,
  },
  scrollContainer: {
    flexGrow: 1,
  },
});
