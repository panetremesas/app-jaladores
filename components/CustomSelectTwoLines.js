import React from 'react';
import { Select, Icon, Box, Text } from 'native-base'; // Importa Text de NativeBase

const CustomSelectTwoLines = ({
  value,
  onValueChange,
  placeholder,
  icon,
  iconSize = 'md',
  iconColor = '#053044',
  iconPadding = 2,
  iconMarginLeft = 5,
  backgroundColor = 'white',
  shadow = 7,
  borderRadius = 30,
  width = '100%',
  items = [],
  ...rest
}) => {
  return (
    <Select
      selectedValue={value}
      onValueChange={onValueChange}
      placeholder={placeholder}
      width={width}
      bg={backgroundColor}
      shadow={shadow}
      borderRadius={borderRadius}
      _selectedItem={{
        bg: backgroundColor,
      }}
      {...rest}
    >
      {items.map((item, index) => (
        <Select.Item key={index} label={item.label} value={item.value} />
      ))}
    </Select>
  );
};

// const MultiLineLabel = ({ label }) => {
//   // Divide el texto en dos líneas usando '\n' como separador
//   const lines = label.split('\n');

//   return (
//     <Box>
//       {lines.map((line, index) => (
//         <Text key={index}>{line}</Text>
//       ))}
//     </Box>
//   );
// };


// const MultiLineLabel = ({ label }) => {
//   // Dividir el texto en líneas usando '\n' como separador
//   const lines = label.split('\n');

//   const multiLineLabel = lines.join('\n');

//   return <Text>{multiLineLabel}</Text>;
// };

export default CustomSelectTwoLines;
