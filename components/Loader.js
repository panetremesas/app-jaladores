import React, { useEffect, useRef } from 'react';
import { View, Animated, StyleSheet, Image } from 'react-native';

const Loader = () => {
  const rotateValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    const rotateAnimation = Animated.loop(
      Animated.timing(rotateValue, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      })
    );

    rotateAnimation.start();

    return () => rotateAnimation.stop();
  }, [rotateValue]);

  return (
    <View style={styles.container}>
      <View style={styles.loaderContainer}>
        <View style={styles.imageContainer}>
          <Image
            source={require('../assets/loader.png')}
            style={styles.imageStyle}
            resizeMode="contain"
          />
        </View>
        <Animated.View
          style={[
            styles.spinner,
            {
              transform: [
                {
                  rotate: rotateValue.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg'],
                  }),
                },
              ],
            },
          ]}
        >
          <View style={styles.spinner} />
        </Animated.View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.9)',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 1,
  },
  loaderContainer: {
    position: 'relative',
    alignItems: 'center',
  },
  imageContainer: {
    position: 'absolute',
    zIndex: 1,
  },
  imageStyle: {
    width: 100,
    height: 100,
  },
  spinner: {
    borderWidth: 20,
    borderColor: 'rgba(0, 0, 0, .1)',
    borderLeftColor: 'transparent',
    width: 120,
    height: 120,
    borderRadius: 60,
  },
});

export default Loader;
