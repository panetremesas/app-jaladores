import React from 'react';
import { StyleSheet, ImageBackground, View } from "react-native";
import { Button, Text } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import { Icon } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

const ProfileButton = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.buttonContainer}>
            <Button shadow={9} style={styles.buttonBack} onPress={() => navigation.navigate('ProfilePage')}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon as={Ionicons} name="person" color={'white'} size="sm" />
                </View>
            </Button>
        </View>
    );
};

export default ProfileButton;

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 10,
    },
    buttonBack: {
        backgroundColor: 'rgba(3, 75, 113, 0.8)',
        borderRadius: 5,
    },
});
