import React from 'react';
import { Modal, Icon, Text } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

const CustomModal = ({
  isOpen,
  onClose,
  iconName = 'alert-circle',
  iconColor = 'red.500',
  iconSize = '6xl',
  title = 'Error',
  errorMessage,
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <Modal.Content>
        <Modal.CloseButton />
        <Modal.Header>{title}</Modal.Header>
        <Modal.Body>
          <Icon
            as={<Ionicons name={iconName} />}
            size={iconSize}
            color={iconColor}
            alignSelf="center"
            marginBottom="2"
          />
          <Text alignSelf="center">{errorMessage}</Text>
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
};

export default CustomModal;