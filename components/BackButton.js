import React from 'react';
import { StyleSheet, ImageBackground, View } from "react-native";
import { Button, Text } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import { Icon } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

const BackButton = ({ title }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.buttonContainer}>
            <Button style={styles.buttonBack} onPress={() => navigation.goBack()}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon as={Ionicons} name="arrow-back" color={'white'} size="sm" />
                    <Text style={{ color: 'white' }}>Atras</Text>
                </View>
            </Button>
        </View>
    );
};

export default BackButton;

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 10,
    },
    buttonBack: {
        backgroundColor: 'rgba(3, 75, 113, 0.8)',
        borderRadius: 25,
    },
});
