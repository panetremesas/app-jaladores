import React from 'react';
import { Button, Icon } from 'native-base';
import { Ionicons } from '@expo/vector-icons'; // Importamos Ionicons

const CustomButton = ({
  onPress,
  title,
  fontWeight = 'bold',
  width = '100%',
  mt = '2',
  mb = '10%',
  borderRadius = 30,
  backgroundColor = 'rgba(3, 75, 113, 0.7)',
  textStyle = { fontWeight: 'bold' },
  icon = null, // Nueva prop para el icono
  iconColor = 'white', // Color del icono (opcional)
  iconSize = 'md', // Tamaño del icono (opcional)
  ...rest
}) => {
  return (
    <Button
      onPress={onPress}
      fontWeight={fontWeight}
      width={width}
      mt={mt}
      mb={mb}
      borderRadius={borderRadius}
      backgroundColor={backgroundColor}
      _text={textStyle}
      leftIcon={
        icon && ( // Si se proporciona un icono, lo renderizamos
          <Icon
            as={<Ionicons name={icon} />} // Utilizamos Ionicons para el icono
            color={iconColor}
            size={iconSize}
          />
        )
      }
      {...rest}
    >
      {title}
    </Button>
  );
};

export default CustomButton;