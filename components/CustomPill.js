import React from 'react';
import { Box, Text } from 'native-base';

const CustomPill = ({ color, text }) => {
  return (
    <Box
      bg={color}
      px={2}
      py={1}
      borderRadius={30}
      alignSelf="flex-start"
    >
      <Text color="white" fontWeight="bold">{text}</Text>
    </Box>
  );
};

export default CustomPill;
