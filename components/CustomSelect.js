import React from 'react';
import { Select, Icon, Box } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

const CustomSelect = ({
  value,
  onValueChange,
  placeholder,
  icon,
  iconSize = 'md',
  iconColor = '#053044',
  iconPadding = 2,
  iconMarginLeft = 5,
  backgroundColor = 'white',
  shadow = 7,
  borderRadius = 30,
  width = '100%',
  items = [],
  ...rest
}) => {
  return (
    <Select
      selectedValue={value}
      onValueChange={onValueChange}
      placeholder={placeholder}
      width={width}
      bg={backgroundColor}
      shadow={shadow}
      borderRadius={borderRadius}
      _selectedItem={{
        bg: backgroundColor,
      }}
      {...rest}
    >
      {items.map((item, index) => (
        <Select.Item key={index} label={item.label} value={item.value} />
      ))}
    </Select>
  );
};

export default CustomSelect;
