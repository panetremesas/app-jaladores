import React, { useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import instanceWithToken from '../utils/httpServiceWithToken';

const AuthManager = () => {
  const navigation = useNavigation();

  useEffect(() => {
    const interceptorId = instanceWithToken.interceptors.response.use(
      (response) => response,
      async (error) => {
        const originalRequest = error.config;
        if (error.response.status === 401 && !originalRequest._retry) {
          originalRequest._retry = true;
          await AsyncStorage.clear();
          navigation.reset({
            index: 0,
            routes: [{ name: 'WelcomePage' }],
          });
        }
        return Promise.reject(error);
      }
    );

    return () => {
      instanceWithToken.interceptors.response.eject(interceptorId);
    };
  }, [navigation]);

  return null; // O puedes renderizar algún componente hijo
};

export default AuthManager;