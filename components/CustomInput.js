import React from 'react';
import { Input, Icon } from 'native-base';
import { Ionicons } from '@expo/vector-icons';

const CustomInput = ({
  value,
  onChangeText,
  onBlur,
  placeholder,
  icon,
  iconSize = 'md',
  iconColor = '#053044',
  iconPadding = 2,
  iconMarginLeft = 5,
  backgroundColor = 'white',
  shadow = '9',
  borderRadius = 30,
  width = 80,
  keyboardType = 'default', 
  ...rest
}) => {
  return (
    <Input
      keyboardType={keyboardType}
      value={value}
      onChangeText={onChangeText}
      onBlur={onBlur}
      placeholder={placeholder}
      width={width}
      InputLeftElement={
        <Icon
          as={<Ionicons name={icon} />}
          size={iconSize}
          m={iconPadding}
          ml={iconMarginLeft}
          color={iconColor}
        />
      }
      bg={backgroundColor}
      shadow={shadow}
      borderRadius={borderRadius}
      {...rest}
    />
  );
};

export default CustomInput;