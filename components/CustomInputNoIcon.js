import React from 'react';
import { Input } from 'native-base';

const CustomInputNoIcon = ({
  value,
  onChangeText,
  placeholder,
  backgroundColor = 'white',
  shadow = '1',
  borderRadius = 5,
  ...rest
}) => {
  return (
    <Input
      value={value}
      onChangeText={onChangeText}
      placeholder={placeholder}
      bg={backgroundColor}
      shadow={shadow}
      borderRadius={borderRadius}
      width={'95%'}
      {...rest}
    />
  );
};

export default CustomInputNoIcon;