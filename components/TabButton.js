import { StyleSheet, View } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { FloatingAction } from "react-native-floating-action";
import { Image } from 'react-native';

export default function TabButton() {
    const actions = [
        {
            text: "Inicio",
            icon: <Image source={require('../assets/icons/home.png')} resizeMode="contain" style={styles.iconsFloat} />,
            name: "HomePage",
            position: 2,
            color: "transparent"
        },
        {
            text: "Crear Nueva",
            icon: <Image source={require('../assets/icons/crear.png')} resizeMode="contain" style={styles.iconsFloat} />,
            name: "CalculatorTransactionPage",
            position: 3,
            color: "transparent"
        },
        {
            text: "Ver Historial",
            icon: <Image source={require('../assets/icons/historial.png')} resizeMode="contain" style={styles.iconsFloat} />,
            name: "HistoryTransactionsPage",
            position: 4,
            color: "transparent"
        },
        {
            text: "Ver Clientes",
            icon: <Image source={require('../assets/icons/clientes.png')} resizeMode="contain" style={styles.iconsFloat} />,
            name: "ClientsPage",
            position: 5,
            color: "transparent"
        },
        {
            text: "Ver Mis Wallets",
            icon: <Image source={require('../assets/icons/historial2.png')} resizeMode="contain" style={styles.iconsFloat} />,
            name: "WalletsPage",
            position: 5,
            color: "transparent",
        },
        {
            text: "Recargas",
            icon: <Image source={require('../assets/icons/recargar.png')} resizeMode="contain" style={styles.iconsFloat} />,
            name: "HistoryRefill",
            position: 5,
            color: "transparent",
        },
        {
            text: "Calculadora",
            icon: <Image source={require('../assets/icons/calculadora.png')} resizeMode="contain" style={styles.iconsFloat} />,
            name: "CalculatorPage",
            position: 5,
            color: "transparent",
        },
    ];

    const navigation = useNavigation();

    return (
        <FloatingAction
            actions={actions}
            onPressItem={name => {
                navigation.navigate(name);
            }}
            color="#034B71"
            buttonSize={60}
            buttonStyle={styles.floatingButton}
            floatingIcon={<Image source={require('../assets/icons/options.png')} style={styles.iconsFloat} />}
        />
    )
}
const styles = StyleSheet.create({
    floatingButton: {
        width: 60,
        height: 60,
        borderRadius: 0, // Esto establece el botón como cuadrado
    },
    iconsFloat: {
        width: '100%',
        height: '100%',
    },
});