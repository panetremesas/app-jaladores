import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import AuthManager from "./components/AuthManager";

// base
import WelcomePage from "./pages/WelcomePage";
import LoginPage from "./pages/LoginPage";
import HomePage from "./pages/HomePage";
import ProfilePage from "./pages/ProfilePage";

// transacciones
import AddTransactionPage from "./pages/transactions/AddTransactionPage";
import ClientReceivePage from "./pages/transactions/ClientReceivePage";
import AddClienInstrumentPage from "./pages/transactions/AddInstrumentClient";
import PayPage from "./pages/transactions/PayPage";
import DetailTransactionPage from "./pages/transactions/DetailTransactionPage";

// utilidades
import CalculatorTransactionPage from "./pages/transactions/CalculatorTransactionPage";

import HistoryTransactionsPage from "./pages/history/HistoryTransactionsPage";
import ClientsPage from "./pages/clients/ClientsPage";
import { DetailClientPagePage } from "./pages/clients/DetailClientPage";
import WalletsPage from "./pages/wallets/WalletsPage";
import TrackingTransactionPage from "./pages/utils/TrackingTransactionPage";
import WalletFillPage from "./pages/refills/WalletFillPage";
import { HistoryRefill } from "./pages/refills/HistoryRefill";
import { DetalleRefillPage } from "./pages/refills/DetalleRefillPage";
import { WithdrawalPage } from "./pages/withdrall/WithdrawalPage";
import { NewWithdrawalPage } from "./pages/withdrall/NewWithdrawalPage";
import * as Notifications from "expo-notifications";
import Constants from "expo-constants";
import * as Device from "expo-device";
import { Platform } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import CalculatorPage from "./pages/utils/CalculatorPage";
import SelectInstrumentPage from "./pages/withdrall/SelectInstrumentPage";

const Stack = createStackNavigator();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

class App extends React.Component {
  render() {
    function handleRegistrationError(errorMessage) {
      alert(errorMessage);
      throw new Error(errorMessage);
    }

    async function registerForPushNotificationsAsync() {
      if (Platform.OS === "android") {
        Notifications.setNotificationChannelAsync("default", {
          name: "default",
          importance: Notifications.AndroidImportance.MAX,
          vibrationPattern: [0, 250, 250, 250],
          lightColor: "#FF231F7C",
        });
      }

      if (Device.isDevice) {
        const { status: existingStatus } =
          await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== "granted") {
          const { status } = await Notifications.requestPermissionsAsync();
          finalStatus = status;
        }
        if (finalStatus !== "granted") {
          handleRegistrationError(
            "Permission not granted to get push token for push notification!"
          );
          return;
        }
        const projectId =
          Constants?.expoConfig?.extra?.eas?.projectId ??
          Constants?.easConfig?.projectId;
        if (!projectId) {
          handleRegistrationError("Project ID not found");
        }
        try {
          const pushTokenString = (
            await Notifications.getExpoPushTokenAsync({
              projectId,
            })
          ).data;
          await AsyncStorage.setItem("pushTokenString", pushTokenString);
        } catch (e) {
          handleRegistrationError(`${e}`);
        }
      } else {
        handleRegistrationError(
          "Must use physical device for push notifications"
        );
      }
    }

    registerForPushNotificationsAsync();

    Notifications.addNotificationReceivedListener((notification) => {
      const { data } = notification.request.content;
    });
    return (
      <NavigationContainer>
        <AuthManager />
        <Stack.Navigator>
          <Stack.Screen
            name="WelcomePage"
            component={WelcomePage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="LoginPage"
            component={LoginPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="HomePage"
            component={HomePage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="AddTransactionPage"
            component={AddTransactionPage}
            options={{
              title: "Datos del cliente",
              headerStyle: {
                backgroundColor: "#053044",
              },
              headerTintColor: "#fff",
            }}
          />
          <Stack.Screen
            name="DetailTransactionPage"
            component={DetailTransactionPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="ProfilePage"
            component={ProfilePage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="ClientReceivePage"
            component={ClientReceivePage}
            options={{
              title: "Seleccionar Instrumento",
              headerStyle: {
                backgroundColor: "#053044",
              },
              headerTintColor: "#fff",
            }}
          />
          <Stack.Screen
            name="AddClienInstrumentPage"
            component={AddClienInstrumentPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="CalculatorTransactionPage"
            component={CalculatorTransactionPage}
            options={{
              title: "Calcular Monto",
              headerStyle: {
                backgroundColor: "#053044",
              },
              headerTintColor: "#fff",
            }}
          />
          <Stack.Screen
            name="PayPage"
            component={PayPage}
            options={{
              title: "Subir Comprobante",
              headerStyle: {
                backgroundColor: "#053044",
              },
              headerTintColor: "#fff",
            }}
          />
          <Stack.Screen
            name="HistoryTransactionsPage"
            component={HistoryTransactionsPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="ClientsPage"
            component={ClientsPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="DetailClientPagePage"
            component={DetailClientPagePage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="WalletsPage"
            component={WalletsPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="TrackingTransactionPage"
            component={TrackingTransactionPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="HistoryRefill"
            component={HistoryRefill}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="WalletFillPage"
            component={WalletFillPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="DetalleRefillPage"
            component={DetalleRefillPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="WithdrawalPage"
            component={WithdrawalPage}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="NewWithdrawalPage"
            component={NewWithdrawalPage}
            options={{
              title: "Hacer un Retiro",
              headerStyle: {
                backgroundColor: "#053044",
              },
              headerTintColor: "#fff",
            }}
          />
          <Stack.Screen
            name="CalculatorPage"
            component={CalculatorPage}
            options={{
              title: "Calculadora",
              headerStyle: {
                backgroundColor: "#053044",
              },
              headerTintColor: "#fff",
            }}
          />

          <Stack.Screen
            name="SelectInstrumentPage"
            component={SelectInstrumentPage}
            options={{
              title: "Selecciona un instrumento",
              headerStyle: {
                backgroundColor: "#053044",
              },
              headerTintColor: "#fff",
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default App;
